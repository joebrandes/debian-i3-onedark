# Polybar

As of testings (2022-07) i couldn't get the standard Debian Polybar Version going.
Meaning: the Polybar version from the standard Repos!

So i go for the Polybar version from the Backports:

*   [Debian Backports](https://wiki.debian.org/Backports)
*   [Polybar Installations](https://github.com/polybar/polybar#installation)

Installprocess automatic with ``installs.sh`` script:

    # Polybar from Backports
    sudo bash -c 'echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" >> /etc/apt/sources.list'
    sudo apt update
    sudo apt -t bullseye-backports install polybar

That should work for my Polybar tech to run.




## General

You can easily have more fun with an easy Window Manager like **i3** or
**openbox**, when you use **Polybar**.

With **Polybar** i generally go for one of two ways:

*   Manually onfigure your own Polybar - ideas are out there - lot's of them, e.g.:

    [Github Repo raven2cz](https://github.com/raven2cz/polybar-config)

*   Go for a **Polybar Themes** technology like

    https://github.com/adi1090x/polybar-themes

*   An example for Polybar in combination with **bspwm** (who cares ;-)

    https://github.com/justTOBBI/dotfiles


## Manual Polybar Start

A few Quickinfos for **Polybar**

*   Get rid of the complete ``bar { ... }`` statement in your ``~/.config/i3/config``
*   Instead use a *launcher* for **Polybar**

I have a working example for Polybar with the standard Polybar config
from there documentation as ``~/.config/polybar`` provided

You have to use the Polybar Config for i3 of course.

The easy way with my docs: change ``~/.xsession`` content:

    exec i3
    # exec i3 -c $HOME/.config/i3/config-i3-POLYBAR-standard

That's all folks to Polybar.

But you can go *crazy* with **Polybar Themes** and own configuration.


## Polybar Themes

I don't offer a complete set of Configuration for this, because those
need more stuff to look out for.

    For those, who one have i go i leave an exemplary ./i3/config with a
    working launch:

    exec_always --no-startup-id $HOME/.config/polybar/launch.sh --blocks

Here are the **Polybar Themes Basics** from there
[Github Repo adi1090x/polybar-themes](https://github.com/raven2cz/polybar-config).

Install following programs on your system before you use these themes.

*   Polybar : Ofcourse, the bar itself

*   Rofi : For App launcher, network, power and style menus

*   pywal : For pywal support

    ``pip3 install pywal``

*   calc : For random colors support

*   networkmanager_dmenu : For network modules

    [Github Repo for networkmanager_dmenu](https://github.com/firecat53/networkmanager-dmenu)
    copy script in $PATH


### Fonts

Here's a list of all fonts used by these themes.

**Text Fonts**

*   Iosevka Nerd Font
*   Fantasque Sans Mono
*   Noto Sans
*   Droid Sans
*   Terminus

**Icon Fonts**

*   Iosevka Nerd Font
    [Nerdfonts Iosevka](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Iosevka)
    and
    [Github Repo be5invis/Iosevka](https://github.com/be5invis/Iosevka)

*   [Icomoon Feather](https://github.com/khanhas/dotfiles/tree/master/polybar/fonts)
*   [Material Icons](https://github.com/google/material-design-icons/tree/master/font)
*   [Waffle (Siji)](https://github.com/stark/siji)


### Installer for Polybar Themes

The Rep has a Built-In Installer for your new Polybar Themes stuff!

It even copies your *Old Polybar Folder* and names it polybar.old!

To-Do:

*   https://gitlab.com/polybar/polybar/-/wikis/Module:-i3
