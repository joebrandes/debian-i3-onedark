#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# CP Polybar stuff
cp -a .config/polybar ~/.config/
echo 'Polybar stuff copied'

# CP Polybar Themes stuff
cp -a .config/polybar-themes ~/.config/
echo 'Polybar Themes stuff copied'
