# BAT / BATCAT

A cat Clone written in Rust: [Git Repo: sharkdp/bat](https://github.com/sharkdp/bat)

![A cat Clone written in Rust](bat-batcat-sharkdp-repo.png)

How often do we need a text displayed properly or
previews for **fzf**. The tool **bat / batcat** is the
way to go.

Refer to Manpage or
[Cyberciti Biz Article "bat Linux command – A cat clone with written in Rust"](https://www.cyberciti.biz/open-source/bat-linux-command-a-cat-clone-with-written-in-rust/)
