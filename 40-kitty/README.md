# Kitty

The *fastest* Terminal Emulator on the market. A bit *old* in Vanilla Debian - but OK.

Easy to configure and masterly documented.

## Kitty Themes

Look no further than https://github.com/dexpota/kitty-themes

