# Ranger



Fun stuff for **Ranger** with **ueberzug** Previews (per pip3 install)


## Using with Kitty Terminal

Better Image preview Performance with Kitty **preview method** than ueberzug!



## Individual Installation (for me included in Basis Install!!)

Install **ueberzug** with ``pip3 install ueberzug`` and Re-Login (for $PATH to take effect).

`sudo apt install python3-pip python3-dev libjpeg-dev zlib1g-dev libxtst-dev`

Check $PATH for ueberzug install in `/home/username/.local/bin`

For Debian just a (complete) logoff and Re-Login!

It should function with Data provided by ``~/.config/ranger/``

**IMPORTANT**

This is an user-specific installation and must be renewed for **NEW User**!

