# Urxvt

A classic Terminal Emulator - the Thing is on all my installations - but it's
a *Love/Hate-Relationship*.

Very hard to configure a to get it going, when it comes to daily stuff like
copy & paste or the handling of Fonts and Icons!

It is depending on a superbly configured ``~/.Xresources`` oder ``~/.Xdefault``.
