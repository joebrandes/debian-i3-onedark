
# Rofi

I am a fan and avid user of Rofi for many years. But i always
tended to use complicated *oneliners* for my keyboard shortcuts
to do the trick. Which is a bit silly really!

So let's build a clean Rofi environment:

*   Rofi Installation easy via Package **rofi** in my Distros
*   Rofi Config: ``~/.config/rofi/config.rasi``
*   Rofi Themes: ``~/.local/share/rofi/themes/*.rasi``






## Old Rofi Stuff

[Github Repo - user - OneDark](https://github.com/davatorium/rofi-themes/blob/master/User%20Themes/onedark.rasi)

I sponsor a few variations in my ``.config/rofi/themes`` Folder.

Without this Themes Folder there is no functioning Rofi with ``Meta`` + ``D``.

Example from i3 config shortcut:

    # rofi - programmes
    bindsym $super+d exec rofi -lines 15 -padding 28 -width 100 -show drun \
        -sidebar-mode -columns 2 -font 'MesloLGS NF 12' -show-icons \
        -theme ~/.config/rofi/themes/$theme
