# Vim - Vi improved

For years the goto *Guy* for editing in the console.

But: with Version 9 on the *Master Bram* of the Vim-Universe decided
to go for a complete new script language instead of using what's out there already.

This will bring many, many problems! And as i always say "I like beeing part
of a solution - not part of a problem".

So i decided to go fully **Neovim - Lua** in the future...
