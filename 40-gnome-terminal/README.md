# Gnome Terminal

Yeah i know: over 120 new packages and over 250 MB on the storage side of things.

But otherwise i have to make a lot of sacrifices using urxvt - and kitty is a bit
on the old side on Debian ...

So, let's do this (my Basis install-script may did it already!!): ``sudo apt install gnome-terminal``

**Styling with OneDark via Standard Colors possible and useful!** (see Color Table on Master-README)


Or user OneDark Theme for Gnome-Terminal - but gconf2 script needs *full* Gnome-Env.

[Get OneDark Theme for Gnome Terminal](https://github.com/denysdovhan/one-gnome-terminal)
(needs at least package **gconf2**)

If you trust Network-/Github-Script do:

``bash -c "$(curl -fsSL https://raw.githubusercontent.com/denysdovhan/gnome-terminal-one/master/one-dark.sh)"``
