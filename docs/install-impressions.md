**Table of contents - TOC**

- [Installation - Installer-Script available](#installation---installer-script-available)
  - [Installer Script](#installer-script)
  - [After full Install](#after-full-install)
  - [Configure the system](#configure-the-system)



#  Installation - Installer-Script available

After Rawinstall Debian 11 checked with ``htop``:

**70 MB RAM with 14 Tasks running!**

![Wow this is tiny - A Debian Basis Install!](../90-PICS/debian-raw.png)

Let's get Window Manager **i3** and **Main / Common Software** going:

```
sudo apt install xorg xinit xsettingsd xterm  aptitude \
    i3 i3status i3lock-fancy python3-i3ipc  \
    firefox-esr evolution evince vlc geany gsimplecal \
    rxvt kitty rofi suckless-tools dzen2 picom dunst \
    ranger python3-pip python3-dev python3-pygit2 \
    python3-psutil python3-netifaces gnome-system-monitor \
    libjpeg-dev zlib1g-dev libxtst-dev \
    zsh fzf ripgrep python3-pygments neofetch \
    fonts-noto fonts-font-awesome \
    highlight hsetroot scrot htop calc figlet arandr nitrogen inxi \
    pcmanfm thunar lxappearance mc vim git tmux curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior \
    yad xdotool xcwd zathura zathura-pdf-poppler zathura-ps
```


## Installer Script

Tip:

Just SSH in the Machine and Copy & Paste or do a ``git clone``
and use the ``installs.sh`` from this Repo! See Quick and Dirty...

Info / Message of Install process: (polybar in separate Backports install)


```
0 aktualisiert, 909 neu installiert, 0 zu entfernen und 0 nicht aktualisiert.
Es müssen 853 MB an Archiven heruntergeladen werden.
Nach dieser Operation werden 2.253 MB Plattenplatz zusätzlich benutzt.
Möchten Sie fortfahren? [J/n]
```

Let's reboot and see what's going on in a near fully functional Debian i3 WM environment:

![Wow still tiny - A Debian Basis Software Deployment!](./90-PICS/debian-after-baseinstall.png)


After Xorg and i3 stuff for Debian: **92 MB RAM with 22 Tasks running!**

Preparation for XServer with `echo 'exec i3' > ~/.xsession` and try `startx`.

WM i3 running with minimal Configuration: **160 MB RAM with 33 Tasks!**

Link:
[More interesting Software stuff and complete usable i3 Enviroment](https://github.com/addy-dclxvi/i3-starterpack)
would need a bit more Fonts (here: package fonts-mplus).


## After full Install

With all that stuff we are coming to **210 MB RAM with 43 Tasks!**

And this es the completed installation process and the resulting i3-Environment:

![i3 (with killed picom for screenshot)](../90-PICS/debian-i3-installed-configured.png)


## Configure the system

Tip:

Just SSH in the Machine and Copy & Paste or do a ``git clone``
and use the ``confs.sh`` from this Repo! See Quick and Dirty...

