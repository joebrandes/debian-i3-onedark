**Table of contents - TOC**

- [Themeing with OneDark](#themeing-with-onedark)
  - [Themeing and To-Do](#themeing-and-to-do)
  - [Themeing files and color processing](#themeing-files-and-color-processing)
  - [Starts / Configuration / Theming files](#starts--configuration--theming-files)


# Themeing with OneDark

    The Repo started out with this themeing idea...

As a Theme Example - just happen to like it right now and
it seems i had a bit to much **Dracula Theme** for the last 2 to 3
years.

![One Dark Color Basic Palette](../90-PICS/OneDarkPalette.png)

For a Look use
[Github Repo for One Gnome Terminal](https://github.com/denysdovhan/one-gnome-terminal/blob/master/COLORS)
or others: (here completed with some Color Adoptions)

```
One Dark                       One Light

Black:          #000000        Black:          #000000

Black (above)   #282C34        Black (above)   #282C34
Bright Black:   #5C6370        Bright Black:   #383A42

Red:            #E06C75        Red:            #E45649
Bright Red:     #E06C75        Bright Red:     #E45649

Green:          #98C379        Green:          #50A14F
Bright Green:   #98C379        Bright Green:   #50A14F

Yellow:         #D19A66        Yellow:         #986801
Bright Yellow:  #E5C07B        Bright Yellow:  #986801

Blue:           #61AFEF        Blue:           #4078F2
Light Blue:     #61AFEF        Light Blue:     #4078F2

Magenta:        #C678DD        Magenta:        #A626A4
Light Magenta:  #C678DD        Light Magenta:  #A626A4

Cyan:           #56B6C2        Cyan:           #0184BC
Light Cyan:     #56B6C2        Light Cyan:     #0184BC

White:          #ABB2BF        White:          #A0A1A7
Bright White:   #FFFFFF        Bright White:   #FFFFFF

Text:           #5C6370        Text:           #383A42
Bold Text:      #ABB2BF        Bold Text:      #A0A1A7
Selection:      #3A3F4B        Selection:      #3A3F4B
Cursor:         #5C6370        Cursor:         #383A42
Background:     #1E2127        Background:     #F9F9F9
```

## Themeing and To-Do

Switching Capability to different Theming.

*   OneDark (Base for this Repo)
*   Nord (Integrated Alternative)
*   Gruvbox - https://github.com/sainnhe/gruvbox-material/wiki/Related-Projects
*   Dracula

Others: (Feel free to complete the infos)

*   Onehalf -  https://github.com/dracula/dracula-theme/tree/master/themes

*   Solarized (not my fav anymore ...)
*   Everforest
*   Purify
*   Monokai
*   Sonokai
*   Material
*   ...

## Themeing files and color processing

Theming via ``~/.Xresources`` - there are different Versions of Xresources
for each Color Theme.

```
10-Xorg-Resources
├── README.md
├── .Xresources
├── .Xresources-nord
├── .Xresources-onedark
├── ...
├── .xsessionrc
└── .xsession
```

The ``.xsession`` is the pure **startx-helper** -
just starts Window Manager i3 - and does give me Environment
with Configuration in ``.xsessionrc``.

Alternatively: we could use ``.xinitrc`` for startx - some Non-Debian
Distros need this alternative!

Configuration: ``.xsessionrc`` (creating to Env-Variables)


    export JBI3THEME="nord"
    export JBI3VAR="i3standard"
    # Example for Line above
    # ------------------------------------------------------
    # Themes: (Variable $JBI3THEME)
    # =======
    # nord
    # onedark
    # gruvbox
    # dracula
    # solarized (no good colortheming)
    # ------------------------------------------------------
    # i3 Variants: (Variable $JBI3VAR)
    # ============
    # i3standard (Standard i3)
    # polybar (own tryout with vanilla polybar)
    # bumblebee (nice python-based status bar helper)
    # polybarthemes (Github Repo Polybar Themes modified)
    # ------------------------------------------------------

    export DESKTOP_SESSION="i3"
    # Set i3 configuration via i3 variants with PART(s)
    cp ~/.config/i3/config-BASE ~/.config/i3/config
    cat ~/.config/i3/config-PART-$JBI3THEME >> ~/.config/i3/config

    # set kitty theming via i3 theme
    cp ~/.config/kitty/kitty-$JBI3THEME ~/.config/kitty/kitty.conf

    setxkbmap -option caps:escape

    xrdb -load ~/.Xresources-$JBI3THEME


For *startx*: ``.xsession`` (starting the i3 with tech-variant)

Important: If you use a Login Manager (GDM, **LightDM**, SDDM) then
this file gets neglected.

    # OLD SOLUTION (now: .xsessionrc):
    # source $HOME/.config/i3/scripts/i3theme.sh nord bumblebee
    # Example for Line 2:
    # source $HOME/.config/i3/scripts/i3theme.sh nord i3standard
    # ------------------------------------------------------
    # Themes: (Variable $1)
    # =======
    # nord
    # onedark
    # gruvbox
    # dracula
    # solarized (no good colortheming)
    # ------------------------------------------------------
    # i3 Variants:
    # ============
    # i3standard (Standard i3)
    # polybar (own tryout with vanilla polybar)
    # bumblebee (nice python-based status bar helper)
    # polybarthemes (Github Repo Polybar Themes modified)
    # ------------------------------------------------------

    exec i3



The colors are provided by ``.Xresources-$theme``
and get loaded in the i3-config(s):

```
# Theming
set $theme onedark

##########
# colors #
##########

...

# get color from xressource configuration - variable_name xressource_color fallback

# special
set_from_resource $foreground foreground #5C6370
set_from_resource $background background #1E2127
set_from_resource $cursorColor cursorColor #5C6370

# black
set_from_resource $black1 color0 #282C34
set_from_resource $black2 color8 #5c6370

# red
set_from_resource $red1 color1 #e06c75
set_from_resource $red2 color9 #e06c75

...

```

In my i3 config i start the tools per Keybindings and use
various configurations for the different Colorstylings!

## Starts / Configuration / Theming files

*   Configuration (ENV) for startx and Login Managers: ``~/.xsessionrc``
*   Start: ``~/.xsession`` (not used with Login-Managers like LightDM)
*   Colors: ``.Xressources-$theme`` (Basic color configuration)
*   i3status Bar: ``.config/i3status/config-$theme``
*   Standard Terminal Kitty: ``~/.config/kitty/kitty-$theme``
*   Neovim: central script ``.config/nvim/init.lua`` loads the rest

    So: Load all the Color Plugins and
    use the color Plugin via ``.config/nvim/lua/user/colorscheme-NAME.lua``
    which gets loadad via line ``require "user.colorscheme-NAME"`` in ``init.lua``
*   Rofi: ``~/.config/rofi/themes/$theme``
*   Midnight Commander: search for skin/Themes

    colors in kitty somehow very themelike!
*   ...
