# Nord or Nordic

A very well documented and support Color Theme.

Sites:

*   [Nord - Main Site](https://www.nordtheme.com/)
*   [Nord - Colors and Palettes](https://www.nordtheme.com/docs/colors-and-palettes)
*   [Nord - Ports (Software Project and SW-Colorthemes)](https://www.nordtheme.com/ports)
*   [Nordtheme Github Repo with more Ports](https://github.com/orgs/nordtheme/repositories)



![Nord Theme Colors](nordtheme-website-allcolors.png)


The Colors are devided on Groups:


**Polar Night** - nord0 to nord3

![Polar Night](polar-night-nord0-nord3.png)

*   #2e3440 (nord0)
*   #3b4252 (nord1)
*   #434c5e (nord2)
*   #4c566a (nord3)

**Snow Storm** - nord4 to nord6

![Snow Storm](snow-storm-nord4-nord6.png)

*   #d8dee9 (nord4)
*   #e5e9f0 (nord5)
*   #eceff4 (nord6)

**Frost** - nord7 to nord10

![Frost](frost-nord7-nord10.png)

*   #8fbcbb (nord7)
*   #88c0d0 (nord8)
*   #81a1c1 (nord9)
*   #5e81ac (nord10)

**Aurora** - nord11 to nord15

![Aurora](aurora-nord11-nord15.png)

*   #bf616a (nord11)
*   #d08770 (nord12)
*   #ebcb8b (nord13)
*   #a3be8c (nord14)
*   #b48ead (nord15)



## Other sources

Nord Theme and Icons - https://github.com/robertovernina/NordArc
