# Themes

Collections of Color Themes for an i3 (or Desktop Environment)
of your liking.

Overview of my favourites: (Website: https://farbenfroh.io/faerber
or here improvised Screenshots [farbenfroh.io Screenshots](./farbenfroh.io-faerber.md) )

*   Onedark

*   Nord

*   Catppuccin

*   Dracula

*   Tokyonight

*   Gruvbox


Each of those Themes and infos to them in the Subfolders.

Please provide Themes and Icons folder:

    mkdir ~/.themes
    mkdir ~/.icons

## Terminal Scripts for Colors / Analysis

I put a couple of Color test scripts in to test the colors in
the various Terminals.


