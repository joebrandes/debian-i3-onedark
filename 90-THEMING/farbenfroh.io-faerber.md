# Farbenfroh.io

Overview of my favourites: (Website: https://farbenfroh.io/faerber)

*   Onedark


*   Nord

    ![farbenfroh.io Nord](Nord/arbenfroh.io-faerber-nord.png)
*   Catppuccin

    ![farbenfroh.io Catppuccin Mocca](Catppuccin/farbenfroh.io-faerber-catppuccion-mocca.png)
*   Dracula

    ![farbenfroh.io Draucula](Dracula/farbenfroh.io-faerber-dracula.png)
    )
*   Tokyonight

    ![farbenfroh.io Tokyonight](Tokyonight/farbenfroh.io-faerber-tokyonight.png)
*   Gruvbox

    ![farbenfroh.io Gruvbox](Gruvbox/farbenfroh.io-faerber-gruvbox.png)

