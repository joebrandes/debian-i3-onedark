# OneDark

or also known as **OneDarkPro**

## Infosites

*   [OneDark Neovim navarasu repo (Style: Cool)](https://github.com/navarasu/onedark.nvim)

    ![OneDark Nvim Style Cool by navarasu](onedark-nvim.png)

*   [OneDark Vim joshdick repo](https://github.com/joshdick/onedark.vim)

    ![OneDark Palette joshdick](onedark-color_reference-joshdick.png)

## GTK-Theme

Styling for GTK is provided by a Git Repo for One Dark with different
Theming from GTK to Cinnamon.

Hint: You find various conf-script for all Icons and Themes

I formerly provided the complete neccessary folderstructures, but that was to much *Git-Hassle*.

So here come the Github Repo with the up to date stuff:
[Github Repo AtomOneDarkTheme](https://github.com/UnnatShaneshwar/AtomOneDarkTheme)

So feel free to ``git clone https://github.com/UnnatShaneshwar/AtomOneDarkTheme`` to your ``~/.themes`` folder.

    # if neccessary:
    # mkdir ~/.themes

    cd ~/.themes && git clone https://github.com/UnnatShaneshwar/AtomOneDarkTheme
    echo 'OneDark Theme cloned'

Changing of GTK Theme and Icons via Tool **lxappearence**.

## Icons

A nice *greenish* Icon Theme for OneDark:

So again just ``git clone https://github.com/adhec/one-dark-icons.git`` to your ``~/.icons`` folder.

    # if neccessary:
    # mkdir ~/.icons

    cd ~/.icons && git clone https://github.com/adhec/one-dark-icons.git
    echo 'One Dark Icons cloned'


## Background Pictures

I Provide Pictures in form of an Extra-Folder ``./90-PICS``.

The setting is done via tool **nitrogen**.
