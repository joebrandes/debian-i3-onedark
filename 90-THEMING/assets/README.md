# Scripts and Stuff

A small sample of Script Examples

*   [Colors 0 to 15 like Neofetch](./colors-0-to-15-from-neofetch.sh) - Code behind link

*   [colortest script for Terminal](./colortest.sh) - Code behind link

*   [color scripts Repo stark/Color-Scripts](https://github.com/stark/Color-Scripts)

    [Test Script Example](./ansi-from-stark-script-repo.sh) - Code behind link
