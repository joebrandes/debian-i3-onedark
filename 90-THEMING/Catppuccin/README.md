# Catppuccin

Catppuccin is a community-driven pastel theme that aims to be the middle ground
between low and high contrast themes.

![Github Repo Demo Pic](catppuccin-demo.png)

Infosites:

*   [Github Catppucin](https://github.com/catppuccin) - Many Ports and Stylings (216 Repos!)
*   [Github Repo Catppucin/Catppuccin](https://github.com/catppuccin/catppuccin)
*   [Catppucin Website](https://catppuccin-website.vercel.app/)


It consists of 4 soothing warm palettes
([4 Palettes](https://github.com/catppuccin/catppuccin#-palette))
with 26 eye-candy colors each, perfect for coding, designing, and much more

*   Latte

    ![Catppuccin Latte](Palettes/catppuccin-latte.png)

*   Frappé

    ![Catppuccin Frappé](Palettes/catppuccin-frappe.png)

*   Macchiato

    ![Catppuccin Macciato](Palettes/catppuccin-macchiato.png)

*   Mocha

    ![Catppuccin Mocha](Palettes/catppuccin-mocha.png)

Link above shows the complete Color Codes (Labels, Hex, RGB, HSL).

📜 License -
Catppuccin is released under the MIT license, which grants the following permissions:

*   Commercial use
*   Distribution
*   Modification
*   Private use


