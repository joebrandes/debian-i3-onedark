# Midnight Commander

MC needs a bit of Skin Help for Theme **One Dark Pro**

[Github Repo with One Dark Theme for MC](https://github.com/DeadNews/mc-onedark)

The Skin should be copied to your own ``~/.local/share/mc/skins``

You can either change to the **One Dark MC Skin** manually inside
the Midnight Commander or copy the Configuration provided in
``.config/mc``.

