```
     ___  ________             ___  ________  _______           ________
    |\  \|\_____  \           |\  \|\   __  \|\  ___ \         |\   __  \
    \ \  \|____|\ /_          \ \  \ \  \|\  \ \   __/|        \ \  \|\ /_
     \ \  \    \|\  \       __ \ \  \ \  \\\  \ \  \_|/__       \ \   __  \
      \ \  \  __\_\  \     |\  \\_\  \ \  \\\  \ \  \_|\ \       \ \  \|\  \ ___
       \ \__\|\_______\    \ \________\ \_______\ \_______\       \ \_______\\__\
        \|__|\|_______|     \|________|\|_______|\|_______|        \|_______\|__|

```
[comment]: # (figlet command: toilet -f ascii9 "Joe Brandes")

# Dotfiles for my specific Machines

For me it makes no sense to have a dotfile managing like **stow**
or Git Tech going for my dotfiles.

Reasons:

*   Different underlying Distros
*   Different working set environments
*   Different toolsets
*   and most importantly: different *Viewports* (Monitorresulutions / Multimonitor)

So i deciede do go for a 3 layer approch:

*   Softwareinstallations - different for various Distros

    look out for install scripts in **99-** folders

*   Basic Configurations - differ *slightly* for various Distros

    look out for configuration scripts and remarks in **99-** folders

*   **Specific Dotfiles** for specific individual Machines

    look out for dotfiles scripts with Machinename and Dates

    e.g.: ``dotfiles-buero2018-20220813``

    or folders with name of machines

    e.g.: ``buero2018/..``



## Candidates for
