# History
HISTFILE=~/.shell_history
HISTSIZE=100000
SAVEHIST=1000
setopt SHARE_HISTORY
#
#
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


# ANTIGEN Solution for Plugins:
# =============================================================================
# https://github.com/zsh-users/antigen
# https://github.com/zsh-users/antigen/wiki/Quick-start 
# curl -L git.io/antigen > antigen.zsh
# or use git.io/antigen-nightly for the latest version
source $HOME/antigen.zsh
# antigen commands: antigen help
# =============================================================================
#
antigen use oh-my-zsh
# plugins in:  ~/.antigen/bundles/robbyrussel/oh-my-zsh/...
antigen bundle git
antigen bundle colorize
antigen bundle colored-man-pages
antigen bundle alias-finder
# plugins in: ~/.antigen/bundles/zsh-users/...
antigen bundle zsh-users/zsh-completions
antigen bundle z
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

# antigen theme robbyrussell
# look ~/.antigen/bundles/romkatv/...
antigen theme romkatv/powerlevel10k
# apply bundles
antigen apply
# =============================================================================


# fpath for lf - a ranger alternative (lf more rare and also for Windows!)
# =============================================================================
# https://linoxide.com/lf-terminal-manager-linux/
# https://github.com/gokcehan/lf/releases
# https://github.com/gokcehan/lf
# completions for tool lf in fpath: fpath in zsh for search of functions
fpath=($HOME/opt/lf-linux-amd64 $fpath)


# use completions:
# =============================================================================
autoload -U compinit && compinit


# FZF - RIPGREP
# =============================================================================
# https://github.com/junegunn/fzf
# fzf environment and exports:
#
. /usr/share/doc/fzf/examples/key-bindings.zsh
. /usr/share/doc/fzf/examples/completion.zsh
export FZF_DEFAULT_OPTS='--height 40% --border'
# use ripgrep with FZF instead of find
# export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'
# more commands:
export FZF_ALT_C_COMMAND='find .'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# configuration for ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# =============================================================================


# User configuration
# =============================================================================
# some Examples from .zshrc installation
#
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#
# =============================================================================


# ZSH-Plugin COLORIZE - see Chapter with antigen above
# =============================================================================
# provides tools colorize_cat (alias ccat) 
# and colorize_less (cless):
#
if [ -n "$(command -v colorize_cat)" ] && [ -n "$(command -v colorize_less)" ]; then
    alias cat='colorize_cat'
    alias less='colorize_less'
fi

# EXA - a modern replacement for ls:
# =============================================================================
# https://the.exa.website/
# https://github.com/ogham/exa
# https://github.com/ogham/exa/releases
#
if [ -x "$(command -v exa)" ]; then
    alias lx='exa --long --tree --level=1 --git'
fi

# COLORLS - a ruby script that colorizes the ls output
# =============================================================================
# https://github.com/athityakumar/colorls
# https://github.com/athityakumar/colorls#installation
# Installation via Ruby Gem: gem install colorls
#
if [ -x "$(command -v colorls)" ]; then
    alias lc='colorls -lA --sd'
fi
# Completion for tool colorls
source $(dirname $(gem which colorls))/tab_complete.sh
# set alias lc for colorls
alias lc='colorls -lAh --sd'


# colors for Terminals normally in
# .Xressources or
# Config-Files of Terminals like alacritty, ...
#
# set dircolors with solarized dark
# eval $(dircolors -b ~/.bash_dircolors.256dark)


# ALIASES
# put my Aliases here - later in extra file?
# =============================================================================

# lf - list files with previewer lfimg
alias lf='lfrun'

# makepasswd - generate and/or encrypt passwords
# Passwords mystyle with Tool makepasswd: 15 Characters out of string
if [ -x "$(command -v makepasswd)" ]; then
    alias jbmakepasswd="makepasswd --chars 15 --string 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ#+-()=}][{;:'"
fi

# imagemagick mogrify 
# alias for web-pics 800px from png to jpg
if [ -x "$(command -v mogrify)" ]; then
    alias jbmogrify="mogrify -path _web-800px/ -resize 800 -quality 90 -format jpg *.png"
fi

# yad - yet another dialog
# fun with yad: change Directory with Dialogue - like my PowerShell cdd
if [ -x "$(command -v yad)" ]; then
    alias cdd='cd $(yad --file --directory)'
fi

# vifm - vim-like file manager
# vifm with ueberzug
alias vifm="~/.config/vifm/vifmrun"

# using ranger with urxvt 
#if [ -x "$(command -v ranger)" ]; then
#    alias ranger="urxvt -e ranger"
#fi


# EXPORTS
# put my Exports here - later in extra file?
# =============================================================================
#
# ZSH Colorize Style
export ZSH_COLORIZE_STYLE="native"

# alternative Editor
export MICRO_TRUECOLOR=1

# fixing Exports for RANGER standard Editor vim
export VISUAL=nvim
export EDITOR=nvim

# FUNCTIONS
# put my Functions here - later in extra file?
# =============================================================================
#
# ghostscript gs function for pdfmerge
pdfmerge() {  
    gs -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -o $@;  
}  


# SPECIALS
# Special Colors for TTY-Console
# 
if [ "$TERM" = "linux" ]; then
	printf %b '\e[40m' '\e[8]' # set default background to color 0 'dracula-bg'
	printf %b '\e[37m' '\e[8]' # set default foreground to color 7 'dracula-fg'
	printf %b '\e]P0282a36'    # redefine 'black'          as 'dracula-bg'
	printf %b '\e]P86272a4'    # redefine 'bright-black'   as 'dracula-comment'
	printf %b '\e]P1ff5555'    # redefine 'red'            as 'dracula-red'
	printf %b '\e]P9ff7777'    # redefine 'bright-red'     as '#ff7777'
	printf %b '\e]P250fa7b'    # redefine 'green'          as 'dracula-green'
	printf %b '\e]PA70fa9b'    # redefine 'bright-green'   as '#70fa9b'
	printf %b '\e]P3f1fa8c'    # redefine 'brown'          as 'dracula-yellow'
	printf %b '\e]PBffb86c'    # redefine 'bright-brown'   as 'dracula-orange'
	printf %b '\e]P4bd93f9'    # redefine 'blue'           as 'dracula-purple'
	printf %b '\e]PCcfa9ff'    # redefine 'bright-blue'    as '#cfa9ff'
	printf %b '\e]P5ff79c6'    # redefine 'magenta'        as 'dracula-pink'
	printf %b '\e]PDff88e8'    # redefine 'bright-magenta' as '#ff88e8'
	printf %b '\e]P68be9fd'    # redefine 'cyan'           as 'dracula-cyan'
	printf %b '\e]PE97e2ff'    # redefine 'bright-cyan'    as '#97e2ff'
	printf %b '\e]P7f8f8f2'    # redefine 'white'          as 'dracula-fg'
	printf %b '\e]PFffffff'    # redefine 'bright-white'   as '#ffffff'
	clear
fi


# END
# if using powerlevel10k then init-/config-testing at the very end

#To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# load the Python3 ipc stuff
# ~/.config/i3/scripts/focus-last.py &


