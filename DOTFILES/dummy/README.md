# dummy folder

Here comes some structure that should be individually
for the **debian-i3** repo-use.

It comes originally from my main multi-monitor machine
in my main office.

Again: just trying to keep tabs on the variations
for dotfiling in my machines.

Remember:

    All initialisations begin in $HOME with the right version of
    .xsessionrc and .xsession (Debian Family) or
    .xinitrc (openSUSE).

After that *all bets are off ;-)*...

## Tree of individuality

The files in question:

    .
    ├── .bashrc
    ├── .config
    │   ├── compton.conf
    │   ├── i3
    │   │   ├── config-BASE
    │   │   ├── config-PART-bumblebee
    │   │   ├── config-PART-i3standard
    │   │   ├── config-PART-polybar
    │   │   └── config-PART-polybarthemes
    │   ├── picom.conf
    │   └── starship.toml
    └── .zshrc


The files in short explanations:

*   ``~/.config/i3/config-BASE`` - individual i3 config

    Programs, Keybindings, Monitor/Workspaces, Compiz Manager

*   ``~/config/i3/config-PART-*`` - individual i3 variants

*   ``~/.config/compton.conf`` (Linux Mint / Ubuntu) or

    ``~/.config/picom.conf`` - (Debian, openSUSE) individual compiz config

*   ``~/.bashrc`` - BASH with different configurations

*   ``~/.zshrc`` - ZSH with different confs

    don't forget Antigen Plugin-Manager for my ZSHs

*   ``~/.config/starship.toml`` - indivdual Starship tech

And one should definitely check for other stuff to complete
the individual environment.

