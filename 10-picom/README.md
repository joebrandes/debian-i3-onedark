# Picom

Doing Window Compositing with **Picom** (instead of compton).

Version on Debian Bookworm (Testing in August 2022): 9.1
from Github Repo https://github.com/yshui/picom

Example ``picom.conf`` see: https://github.com/yshui/picom/blob/next/picom.sample.conf

Tip: Change *vsync to false* if picom has problem starting.

See Youtube Video for working picom:

https://www.youtube.com/watch?v=t6Klg7CvUxA

