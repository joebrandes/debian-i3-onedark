**Table of contents - TOC**

- [Debian with i3 Window Manager](#debian-with-i3-window-manager)
  - [Deploy Distro - Start your TryOuts](#deploy-distro---start-your-tryouts)
  - [Overview - Topics](#overview---topics)
  - [Debian 11/12 - Quick and Dirty - in approx. 15 min from Zero to Hero](#debian-1112---quick-and-dirty---in-approx-15-min-from-zero-to-hero)
  - [Workspaces](#workspaces)
  - [More documentation](#more-documentation)

Motto / Slogan for this Repo: **Debian** with **Window Manager I3** and **Theming**

```

     ________  _______   ________          ___  ________             ___  ________  _______           ________
    |\   ___ \|\  ___ \ |\   __  \        |\  \|\_____  \           |\  \|\   __  \|\  ___ \         |\   __  \
    \ \  \_|\ \ \   __/|\ \  \|\ /_       \ \  \|____|\ /_          \ \  \ \  \|\  \ \   __/|        \ \  \|\ /_
     \ \  \ \\ \ \  \_|/_\ \   __  \       \ \  \    \|\  \       __ \ \  \ \  \\\  \ \  \_|/__       \ \   __  \
      \ \  \_\\ \ \  \_|\ \ \  \|\  \       \ \  \  __\_\  \     |\  \\_\  \ \  \\\  \ \  \_|\ \       \ \  \|\  \ ___
       \ \_______\ \_______\ \_______\       \ \__\|\_______\    \ \________\ \_______\ \_______\       \ \_______\\__\
        \|_______|\|_______|\|_______|        \|__|\|_______|     \|________|\|_______|\|_______|        \|_______\|__|

```

[Figlet Example Site for 3D-ASCII Effect](https://patorjk.com/software/taag/#p=display&f=3D-ASCII&t=I3%20Joe%20B.)

# Debian with i3 Window Manager

Remark: I started out with **Debian 11 Bullseye** and added more Distros and obviously
the next **Debian 12 Bookworm**.

New documentversion in **Summer of code 2022** -
changed from WM Xmonad to **i3** for

*   **Quick Installs** and
*   **Quick Configurations**.

The name of Repo can be *confusing* or *misleading* even,
because these files can give me at least the following Themes

*   Onedark
*   Nord
*   Catppuccin
*   Dracula
*   Gruvbox
*   other themes ...

in combination with various **i3 Status Bars**:

*   i3status/i3bar
*   bumblebee-status
*   polybar
*   polybar-themes

Feel free to adapt and/or Copy & Paste - and the name of the game
is always **Debian i3 Joe B. Style**.




## Deploy Distro - Start your TryOuts

*   Installation of Debian (with Netinstaller):

    openSSH and Systemtools (approx. 119 packages)

*   Alternatively any other Distro

    *   **Debian Testing** - Development Edition Debian 12
    *   **Linux Mint LMDE 5**, which combines **Cinnamon** and **i3** wonderfully.
    *   **openSUSE**, because of some of my educational seminars
        are with **openSUSE**.

    You will find the neccessary  stuff for further testing and analysis in Subfolders.


## Overview - Topics

*   Window Manager **i3** (here: various status bar solutions)
*   Composite Manager **picom** (or **compton**)
*   Coloring terminals and Co with **.Xresources**
*   X Server file examples: **.xsessionrc**, **.xsession** (or **.xinitrc)
*   Shells:
    *   **Bash**
    *   **Zsh** (Zsh with Antigen Plugin-Management)
*   Terminal Emulators:
    *   **kitty** (my favorite right now)
    *   **urxvt**
    *   **xterm**
    *   or gnome-terminal, konsole later for comfort or even tmux)
*   Prompt with **Starship.rs** - the universal prompt for **every** Shell
*   Editors:
    *   **Vim**
    *   **Neovim** (my favorite; look for newer version or Appimage)
*   Tools:
    *   **Rofi** - the Swiss Knife for user interaction and menus (see also dmenu2)
    *   **Ranger**
    *   **Midnight Commander**
*   MyFonts: (a few examples - for folder: ~/.fonts and ~/.local/share/fonts)
    *   **MesloLGS NF**
    *   **FiraCode**
    *   **SauceCode** (aka SourceCode)
    *   **Iosevka** (for Bars like bumblebee-status or i3status/i3bar)
    *   and div others
*   Theming:
    *   GTK-Themes (for folder: ~/.themes)
    *   Icons (for folder: ~/.icons )
*   Pics:
    *   for this Repo and for
    *   Backgrounds



## Debian 11/12 - Quick and Dirty - in approx. 15 min from Zero to Hero

A complete Debian NetInstall incl. Updates and the following configurations in **about 10 to 15 Minutes**!

*   Make Git-Repo-Workdir:

    ``mkdir ~/gitlab-projekte && cd ~/gitlab-projekte``

*   Install Git: (mostly already available/installed)

    ``sudo apt install git``

*   Clone this Repo:

    ``git clone https://gitlab.com/joebrandes/debian-i3-onedark``

*   Software with (**here as an example - look for specific files/folders**)

    ``sh 00-installs...all.sh`` - Scripts in chronological/alphabetical order with Reboot(s)

    Status for Debian 12 Bookworm:

        Es müssen 924 MB an Archiven heruntergeladen werden.
        Nach dieser Operation werden 3.215 MB Plattenplatz zusätzlich benutzt.

    So it may take a few minutes... Machine takes approx 140 MB Mem without X-Server!

*   Configurations with (**here as an example - look for specific files/folders**)

    ``sh 01-confs-and-fonts...all.sh`` - Scripts in chronological order

*   **arandr** (after **startx** of course)

    Screen Resolution with tool **arandr** save ``~/.screenlayout/1920x1080.sh``

*   **nitrogen**

    Background Pic with tool **nitrogen** (also: script available for diffBG/WS)

*   **lxappeararnce**

    GTK Theming with tool **lxappearance** run configure Theme and Icons

*   **vim** and do ``:PlugInstall`` - Re-start vim or get
*   **nvim** (Neovim with Packer or LayzVim!) and do *nothing* ;-)
*   **zsh** introduce and test **zsh** - change your Standard-Shell?!
*   Configure i3 the way you want?!?!
*   Have Fun!


## Workspaces

The idea is to establish a set of Workspaces for my *daily Driver Environment*:

1. **Browser**

   Standard: Firefox

1. **Dateien**

   Linux: Nemo

   Windows: Explorer

1. **Konsolen**

   Linux: Gnome Terminal (gnome-terminal)

   Windows: Windows Terminal (wt)

1. **Coding**

   Linux: VSCodium

   Windows: VSCode

1. **VMS**

   Linux: VirtualBox bzw. KVM/qemu

   Windows: VirtualBox bzw. Hyper-V

1. **Office**

   Linux: OnlyOffic bzw. LibreOffice

   Windows: Office 365 bzw. OnlyOffice

1. **Mail**

   Linux: Evolution

   Windows: Outlook

1. **Graphics**

   Linux: Gimp

   Windows: Affinty

1. **System**

   Linux: Systemtools

   Windows: Systemtools


Obviously just Examples of what could be used as Software on the Workspaces.

Remark:

    There are Versions in Use with only 4 Workspaces.
    In seminars for Example!

## More documentation

*   [Install Impressions](docs/install-impressions.md)
*   [Themeing Impressions](docs/themeing-impresssions.md)
