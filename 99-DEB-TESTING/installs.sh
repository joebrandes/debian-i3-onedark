#!/bin/bash
# install it ALL - on your own ... - i hope you know what you are doing ;-)
# just ssh in your new Machine and copy & Paste or git clone this Repo before
# rxvt-unicode (instead of rxvt)
# neovim polybar (from main)

# Update your system
sudo apt update && sudo apt upgrade -y

# Software installs
sudo apt install xorg xinit xsettingsd xterm  aptitude \
    i3 i3status i3lock-fancy python3-i3ipc  \
    firefox-esr evolution evince vlc geany gsimplecal \
    rxvt-unicode kitty rofi suckless-tools dzen2 picom dunst \
    ranger python3-pip python3-dev python3-pygit2 python3-venv \
    python3-psutil python3-netifaces gnome-system-monitor \
    libjpeg-dev zlib1g-dev libxtst-dev libnotify-bin \
    zsh fzf ripgrep python3-pygments python3-pil neofetch \
    fonts-noto fonts-font-awesome bat bumblebee-status polybar \
    highlight hsetroot scrot htop calc figlet arandr nitrogen inxi \
    pcmanfm thunar lxappearance mc vim git tmux curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior ueberzug \
    yad xdotool xcwd zathura zathura-pdf-poppler zathura-ps


# install ueberzug - uses ~/.local/bin
# pip3 install ueberzug

# install bumblebee-status - in Debian 12 as package!
# pip3 install bumblebee-status

# Starship
curl -sS https://starship.rs/install.sh | sh

# Reboot
sudo reboot

