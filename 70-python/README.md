# Python3 Environment

We need Python3 Virtual Environments today (after 3.9 onwards?)
and there are lot's of Tutorials out there.

So give yourself a good start with the following examples:

## Python3 install

```
# Installs
python3-full python3-pip
```

## VENV (Virtual Env)

```
# Basedir for Virtual env
mkdir -p $HOME/.venvs

# make a new Dir a new VENV Home
python3 -m venv $HOME/.venvs/MyEnv

# don't forget to tell your shell
# source ~/.venvs/MyEnv/bin/activate
```

## Python Modules with pip - no extra Software needed like pipx

```
# install your beloved Modules/Python-Tools
python3 -m pip install esbonio
# sometimes older versions for RestructuredText Themes
python3 -m pip install sphinx==6.2.1
# and more stuff like...
python3 -m pip install sphinxcontrib-mermaid
```

## Example VSCode Settings - settings.json

```
{
    "esbonio.sphinx.buildDir": "${workspaceFolder}/_build",
    
    "esbonio.sphinx.confDir": "${workspaceFolder}",
    "restructuredtext.preview.name": "sphinx",
    "restructuredtext.linter.run": "off"
}
```
