# lazy.nvim

New start with RAW lazy.nvim - not LazyVim!!

From Lazy Control Center:

```
  Total: 13 plugins

  Loaded (13)
    ● catppuccin 4.35ms  start
    ● dashboard-nvim 1.1ms  VimEnter
    ● lazy.nvim 4.03ms  init.lua
    ● lualine.nvim 0.03ms  start
    ● mini.nvim 0.22ms  start
    ● neo-tree.nvim 1.46ms  start
    ● nui.nvim 0.21ms  neo-tree.nvim
    ● nvim-treesitter 8.45ms  start
    ● nvim-web-devicons 0.34ms  neo-tree.nvim
    ● plenary.nvim 0.33ms  neo-tree.nvim
    ● telescope.nvim 0.4ms  start
    ● tokyonight.nvim 0.06ms  start
    ● which-key.nvim 0.85ms  VeryLazy
```
