#!/bin/bash
# For Debian 11 we grab a non invasiv AppImage for testing:
# you may check for the latest Version!)
echo 'Neovim Appimage from Github - Version Stable'
cd ~/.local/bin
wget https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
chmod u+x nvim.appimage
ln -s nvim.appimage nvim
