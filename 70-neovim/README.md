# Neovim

More to come ... - I am switching from Vim (finally) ...

I will start with a configuration i tried out a while ago. We will see
where that leeds me/us.

Latest Version (at the time of this documentation): **Nvim  v0.9.0**

**PROBLEMS**:

*   Version from Debian 11 Sources:
    **neovim 0.4.4-1** (we like/need at least a v0.5 Version
    and right now 0.9 would be much needed for the modern Plugin-stuff!
*   Version from Debian 12 (Testing) Sources:
    **neovim 0.7.2-7** (better, but not enough)

Links:

*   [Neovim.io Portal](https://neovim.io)
*   [Neovim Github](https://github.com/neovim/neovim)
*   [Neovim Install Docs](https://github.com/neovim/neovim/wiki/Installing-Neovim)
*   [Neovim Install by Package Managers](https://github.com/neovim/neovim/wiki/Installing-Neovim)
*   [Neovim Install from Sources](https://github.com/neovim/neovim/wiki/Installing-Neovim#install-from-source)
*   [Neovim Latest Stable](https://github.com/neovim/neovim/releases/latest)
*   [Neovim Releases](https://github.com/neovim/neovim/releases)
*   [Awesome Neovim](https://github.com/rockerBOO/awesome-neovim)

## AppImage installation

For Debian 11 we grab a non invasive AppImage for testing: (you may check the stable Version!)

The following path (and solution) just an example.

```
cd ~/.local/bin
wget https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
chmod u+x nvim.appimage
ln -s nvim.appimage nvim
```

The AppImage can be integreted in your DE or WM.

E.g. Link [linuxconfig.org - how to create ...](https://linuxconfig.org/how-to-create-an-integrated-application-launcher-for-an-appimage-file-in-ubuntu)


## Neovim configuration: kickstart.nvim

* [YouTube Vid Kickstart.nvim TJ de Vries](https://www.youtube.com/watch?v=m8C0Cq9Uv9o&t=163s)
* [Github Repo kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)

Especially be reminded of tech to separate diferent Neovim Configs:

`git clone https://github.com/nvim-lua/kickstart.nvim.git ~/.config/nvim-kickstart`

With an Alias in your Shell config like:

`alias nvim-kickstart='NVIM_APPNAME="nvim-kickstart" nvim'`

That gives you `nvim-kickstart` to use `nvim` with a special conf-Env.

```
_ ZSH ❯ ls -Al ~/.config/nvim* -d
drwxr-xr-x 4 joeb joeb 4096 17. Mär 17:17 /home/joeb/.config/nvim
drwxr-xr-x 6 joeb joeb 4096 24. Mär 14:28 /home/joeb/.config/nvim-kickstart

_ ZSH ❯ ls -Al ~/.local/share/nvim* -d
drwxr-xr-x 6 joeb joeb 4096 18. Mär 17:19 /home/joeb/.local/share/nvim
drwxr-xr-x 4 joeb joeb 4096 24. Mär 14:28 /home/joeb/.local/share/nvim-kickstart
```

You can do this with any other form of Neovim Configurations/Distros!




## Neovim Tech and Plugins

The right way: going full **Lua**! So the basic conf is ``~/.config/nvim/init.lua``

Structure:

```
nvim
├── lua
│   └── user
```

## Neovim Plugin Management and Start configs

*   [LazyVim by Folke Lemaitre Website](https://www.lazyvim.org/) -
    [LazyVim Github Repo](https://github.com/LazyVim/LazyVim) -
    [LazyVim Installation/Deployment](https://www.lazyvim.org/installation) -
    [LazyVim Starter Repo](https://github.com/LazyVim/starter)

    Note to me: **I have my own LayzVim-Config Starter Repo to pull from**

        git clone https://github.com/joebrandes/lazyvim-config.git ~/.config/nvim

    ![LazyVim](lazyvim-alpha.png)

*   [wbthomason/packer.nvim](https://github.com/wbthomason/packer.nvim)

    A full blown Plugin Manager

    Packer starts with a git cloning:

    ```
    git clone --depth 1 https://github.com/wbthomason/packer.nvim \
    ~/.local/share/nvim/site/pack/packer/start/packer.nvim
    ```
    and offer the neccessary code in our config ``~/.config/nvim/init.lua``.

*   [NvChad/NvChad](https://github.com/NvChad/NvChad)

    NvChad is a neovim config written in lua aiming to provide a base configuration
    with very beautiful UI and blazing fast startuptime

*   The list goes on and on ...





## Standard Plugins

Two standard Plugins to optimize the Neovim tech by **TJ DeVries**:

*   [nvim-lua/plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
*   [nvim-lua/popup.nvim](https://github.com/nvim-lua/popup.nvim)

The first provide many functions (in lua) which get used a lot
by other Neovim Plugins. The second one is a modernization of
the *old vim Popup API*.

### Colorschemes, Lualine and colorizer

*   [folke/tokyonight.nvim](https://github.com/folke/tokyonight.nvim) -
    Tokyo Night
*   [ellisonleao/gruvbox.nvim](https://github.com/ellisonleao/gruvbox.nvim) -
    Gruvbox
*   [shaunsingh/nord.nvim](https://github.com/shaunsingh/nord.nvim) -
    Nord
*   [Mofiqul/dracula.nvim](https://github.com/Mofiqul/dracula.nvim) -
    Dracula
*   [navarasu/onedark.nvim](https://github.com/navarasu/onedark.nvim) -
    One Dark
*   [ishan9299/nvim-solarized-lua](https://github.com/ishan9299/nvim-solarized-lua) -
    Solarized (could be removed)

And use **Lualine** with Icons

*   [nvim-lualine/lualine.nvim](https://github.com/nvim-lualine/lualine.nvim) -
    Lualine - Neovim Statusline written in Lua
*   [kyazdani42/nvim-web-devicons](https://github.com/kyazdani42/nvim-web-devicons) -
    a Lua Fork of vim-devicons

Make color infos shine:

*   [norcalli/nvim-colorizer.lua](https://github.com/norcalli/nvim-colorizer.lua) -
    Colorizer - on some files switch in manually on with ``:ColorizerToggle``
    or ``:ColorizerAttachToBuffer``

### Completions

*   [hrsh7th/nvim-cmp](https://github.com/hrsh7th/nvim-cmp) -
    The completion plugin
*   [hrsh7th/cmp-buffer](https://github.com/hrsh7th/cmp-buffer) -
    buffer completions
*   [hrsh7th/cmp-path](https://github.com/hrsh7th/cmp-path) -
    path completions
*   [hrsh7th/cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline) -
    cmdline completions
*   [saadparwaiz1/cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip) -
    snippet completions
*   [hrsh7th/cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp) -
    cmp lsp

### Snippets

*   [L3MON4D3/LuaSnip](https://github.com/L3MON4D3/LuaSnip) -
    snippet engine
*   [rafamadriz/friendly-snippets](https://github.com/rafamadriz/friendly-snippets) -
    a bunch of snippets to use

### LSP

*   [neovim/nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) -
    enable LSP
*   [williamboman/nvim-lsp-installer](https://github.com/williamboman/nvim-lsp-installer) -
    simple to use language server installer

### Telescope

*   [nvim-telescope/telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)  -
    Telescope
*   [nvim-telescope/telescope-media-files.nvim](https://github.com/nvim-telescope/telescope-media-files.nvim)


## Top Repo: LunarVim/nvim-basic-ide

A great idea from Christian Chiarulli (aka LunarVim Creator):

Define a Neovim Installation and go from there with a config that
is frozen. The Freeze is provided by two strategies:

*   Build a definitive Version of Neovim from Code
*   Use **commits** for all Plugins

Infos and Links:

*   [Github Repo Lunarvim/nvim-basic-ide](https://github.com/LunarVim/nvim-basic-ide)
*   Youtube Series: Neovim - creating an unbreakable IDE config | chris@machine
    *   Part 1 https://www.youtube.com/watch?v=Vghglz2oR0c
    *   Part 2 https://www.youtube.com/watch?v=HNQpLyRIreA
    *   Part 3 https://www.youtube.com/watch?v=EGEUwtyUS9w


