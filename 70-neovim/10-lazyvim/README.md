# LazyVim by Folke Lemaitre

This is my goto solution for new installations
(**with my own Private LazyVim Config Starter Repo**)

*   Works OK out of the box

    ``:checkhealth`` on fresh Systems can be a bit daunting

*   Very good Documentation:

    [LazyVim by Folke Lemaitre Website](https://www.lazyvim.org/) -
    [LazyVim Github Repo](https://github.com/LazyVim/LazyVim) -
    [LazyVim Installation/Deployment](https://www.lazyvim.org/installation) -
    [LazyVim Starter Repo](https://github.com/LazyVim/starter)

![LazyVim](../lazyvim-alpha.png)


## Start LazyVim

Make a backup of your current Neovim files:

    # required
    mv ~/.config/nvim ~/.config/nvim.bak-$(date -I)

    # optional but recommended
    mv ~/.local/share/nvim ~/.local/share/nvim.bak-$(date -I)
    mv ~/.local/state/nvim ~/.local/state/nvim.bak-$(date -I)
    mv ~/.cache/nvim ~/.cache/nvim.bak-$(date -I)

Clone the starter

    # the LazyVim Starter provided by Folke:
    git clone https://github.com/LazyVim/starter ~/.config/nvim
    # your own Repo: (normally via ssh with clone git@github.com...)
    git clone https://github.com/joebrandes/lazyvim-config ~/.config/nvim

If you use Folkes Starter: Remove the .git folder,
so you can add it to your own repo later

    rm -rf ~/.config/nvim/.git

Start Neovim!

    nvim

Refer to the comments in the files on how to customize LazyVim.

Tip: It is recommended to run ``:checkhealth`` after installation
