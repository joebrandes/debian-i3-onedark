local ok, _ = pcall(require, 'onedark')
if not ok then
  return
end

require('onedark').setup {
    style = 'darker'
}
require('onedark').load()

require('lualine').setup()
