vim.o.background = "dark"
vim.cmd([[colorscheme gruvbox]])

require('lualine').setup()
