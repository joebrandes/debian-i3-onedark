vim.cmd([[colorscheme dracula]])
require('lualine').setup {
  options = {
    theme = 'dracula-nvim'  
  }
}
