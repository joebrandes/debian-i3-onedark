require('onedark').setup {
    style = 'darker'
}
require('onedark').load()

require('lualine').setup()
