theme = os.getenv("JBI3THEME")
-- or static: theme = "nord"
-- To-Do: fallback to nord with if switches!

require('user.options')
require('user.keymaps')
require('user.plugins')

if( theme == "onedark" ) then require('user.colorscheme-onedark')
elseif( theme == "nord" ) then require('user.colorscheme-nord')
elseif( theme == "gruvbox" ) then require('user.colorscheme-gruvbox')
elseif( theme == "dracula" ) then require('user.colorscheme-dracula')
elseif( theme == "solarized" ) then require('user.colorscheme-solarized')
elseif( theme == "tokyonight" ) then require('user.colorscheme-tokyonight')
else require('user.colorscheme-onedark')
end

require('user.colorizer')
require('user.cmp')
require('user.lsp')
require('user.telescope')

