#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# OneDark
# Nord
# Dracula
# Gruvbox

# GTK Theming / Icons / Pictures

mkdir ~/.icons
mkdir ~/.themes

cd ~/.themes && git clone https://github.com/UnnatShaneshwar/AtomOneDarkTheme
echo 'OneDark Theme cloned'

cd ~/.themes && git clone https://github.com/EliverLara/Nordic
echo 'Nordic Theme cloned'

cd ~/.themes && git clone https://github.com/dracula/gtk.git dracula
echo 'Dracula Theme cloned'

cd ~/tmp && git clone https://github.com/TheGreatMcPain/gruvbox-material-gtk.git gruvbox
cp -a ~/tmp/gruvbox/themes/Gruvbox-Material-Dark ~/.themes/
echo 'Gruvbox Theme cloned'

# GTK Icons

cd ~/.icons && git clone https://github.com/adhec/one-dark-icons.git
echo 'One Dark Icons cloned'

cd ~/tmp && git clone https://github.com/robertovernina/NordArc nordarc
cd nordarc
cp -a NordArc-Icons ~/.icons/
cp -a NordArc-Theme ~/.themes/
echo 'Nordarc Theme and Icons cloned'

cd ~/.icons && git clone https://github.com/m4thewz/dracula-icons.git
echo 'Dracula Icons cloned'

cp -a ~/tmp/gruvbox/icons/Gruvbox-Material-Dark ~/.icons/
echo 'Gruvbox Theme cloned'

