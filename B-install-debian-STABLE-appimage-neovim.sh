#!/bin/bash

# For Debian 11 (or others) we grab a non invasiv AppImage for testing:
# you may also check for the latest Version!)
echo 'Neovim Appimage from Github - Version stable (e.g. 0.7.2)'
cd ~/.local/bin
# use tag stable
wget https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
chmod u+x nvim.appimage
ln -s nvim.appimage nvim
