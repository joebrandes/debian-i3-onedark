# Tmux

Terminal Multiplexer - the professional Way...

I handle **Tmux** with his own Plugin-Handler: **Tmux-Power**

Go for the Repo and prepare your Configurations

    mkdir -p ~/.tmux/plugins
    git clone https://github.com/wfxr/tmux-power.git ~/.tmux/plugins/tmux-power
    # Change with my tmux-power.tmux conf
    cp ~/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux-original
    cp 30-tmux/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux
    # do not forget you ~/.tmux.conf
    cp 30-tmux/.tmux.conf ~/.tmux.conf

Configuration for Tmux-Power at the end of ``~/.tmux.conf``

    # manually set and run - colorscheme gold copied and changed to dracula
    set -g @tmux_power_theme 'dracula'
    run-shell "~/.tmux/plugins/tmux-power/tmux-power.tmux"
