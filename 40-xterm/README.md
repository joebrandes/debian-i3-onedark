# Xterm

The granddad/grandma of all Terminal Emulators. You should have it on board.

Since **Xterm** can use **True Type Fonts** i don't get into the **UXterm**
alternative.

## Config

Just look no further than your good old ``~/.Xresources``.

And after configuration changes don' forget ``xrdb -load ~/.Xresources``


Remark: look for Xterm configuration Example:
https://gist.github.com/taviso/a4543b1752fba55017e8fcc2fe052c0a




## Tipps & Tricks

Use ``ctrl`` + ``Left/Middle/RightClickHold`` for extra menus!


## Copy and Past stuff

This may work: 
https://www.reddit.com/r/linuxquestions/comments/123bqo8/xterm_copy_and_paste_not_working_exactly/

Snippet from Reddit-Post:

XTerm copy and paste not working exactly

Hey guys, I'm trying to figure out how to get this copy and paste thing 
working with Xterm and ~/.Xresources just the way 
I want it to, but I'm not fully 100% understanding it. 
I already know there are two clipboard in Xorg called PRIMARY and CLIPBOARD. 

Here's what the relevant sections of my `~/.Xresources` config looks now.

```
XTerm.VT100.translations: #override \n\
    Ctrl <Key> minus: smaller-vt-font() \n\
    Ctrl <Key> plus: larger-vt-font() \n\
    Ctrl <Key> 0: set-vt-font(d) \n\
    Ctrl Shift <Key>C: copy-selection(SELECT) \n\
    Ctrl Shift <Key>V: insert-selection(SELECT) \n\
    Ctrl Shift <Btn1Up>: exec-formatted("xdg-open '%t'", PRIMARY) \n\
    <Btn1Up>: select-end(PRIMARY) \n\
    <Btn2Up>: insert-selection(PRIMARY)
```

And here is **EXACTLY** how I would like this to behave:

When the setting **XTerm*selectToClipboard** is set to **false**:

*   Selecting from XTerm should: Copy to PRIMARY, NOT CLIPBOARD
*   Shift-Insert/MMB should: Paste from PRIMARY, NOT CLIPBOARD
*   Ctrl-Shift-C should: Copy to PRIMARY but NOT CLIPBOARD
*   Ctrl-Shift-V should: Paste from the PRIMARY, NOT CLIPBOARD

When the setting **XTerm*selectToClipboard** is set to **true**:

*   Selecting from XTerm should: Copy to PRIMARY, NOT CLIPBOARD (Like a regular program)
*   Shift-Insert/MMB should: Paste from PRIMARY, NOT CLIPBOARD (Like a regular program)
*   Ctrl-Shift-C should: Copy to CLIPBOARD but NOT PRIMARY
*   Ctrl-Shift-V should: Paste from the CLIPBOARD, NOT PRIMARY

So:

```
XTerm*selectToClipboard: true
XTerm*keepClipboard: true
```
