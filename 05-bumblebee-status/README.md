# Bumblebee-Status

Yes i know i deliver some stuff for bumblebee, but please use the
install via ``pip3 install bumblebee-status``.

Links:

*   [Github Repo - bumblebee-status](https://github.com/tobi-wan-kenobi/bumblebee-status)
*   [ReadTheDocs - bumblebee-status](https://bumblebee-status.readthedocs.io/en/main/index.html)


## Unicode Separator

See: https://github.com/ryanoasis/powerline-extra-symbols

It needs Nerdfont support - like mine with **Iosevka**.

![Glyphs - see Powerline Docs](../90-PICS/glyphs-for%20powerline_fontforge.png)

In a Combination of **Nevim** in **Kitty** (Terminal) i get
the Unicode Symbols easyly in by ``Shift`` + ``Ctrl`` + ``u``
followed by the Code of the Symbol.

Or do some Shell-stuff with **printf**:

```
    # do an unicode print variable
    D=$(printf "\ue0b2")
    # print it
    echo $D
    
```

## Handling xdg-open for clicks in Bar

For various modules of bumblebee ``xdg-open`` is used. So we have to have
our **Standard Applications** in order!

Status:

```
    ❯ xdg-settings --list
    Known properties:
        default-url-scheme-handler    Default handler for URL scheme
        default-web-browser           Default web browser
```

```
    ❯ cat ~/.config/mimeapps.list
    [Added Associations]
    application/epub+zip=calibre-ebook-viewer.desktop;
    application/x-partial-download=userapp-virt-viewer-IO7SO1.desktop;
    application/octet-stream=xed.desktop;

    [Default Applications]
    application/octet-stream=xed.desktop
```

Add a handler - here: Standard Filemanager

```
    ❯ xdg-mime default nemo.desktop inode/directory
```

Remark: ``nemo.desktop`` for Desktop Env are stored in ``/usr/share/applications/

Now it shows:

```
    ❯ cat ~/.config/mimeapps.list
    [Added Associations]
    application/epub+zip=calibre-ebook-viewer.desktop;
    application/x-partial-download=userapp-virt-viewer-IO7SO1.desktop;
    application/octet-stream=xed.desktop;

    [Default Applications]
    application/octet-stream=xed.desktop
    inode/directory=nemo.desktop
```

Finding out the right Mime-Type: ``xdg-mime query filetype ~/Bild1.jpg`` (shows: image/jpeg)

