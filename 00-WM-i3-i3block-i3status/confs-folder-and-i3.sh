#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# Prepare local folder
mkdir ~/bin
mkdir ~/.local/bin
mkdir ~/opt
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/tmp


# CP i3 stuff
cp -R .config/i3* ~/.config/
# cp .local/bin/autotiling ~/.local/bin/
echo 'i3 stuff copied'
