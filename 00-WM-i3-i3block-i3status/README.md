# i3 Window Manager

Here comes the Vanilla **i3** Window Manager with various Bars & Theming.

## To-Do

The Window Manager **i3** is undergoing development, which makes him
attractive i think. Just put my finger on topics that should be
integrated in later stages of this repo.

*   **Include Directive** (since i3 v4.20 - so no use in Debian 11)

    My Config nowadays is a massive Chunk of Lines that is split
    right in before the Bar Management at the end of i3 config file.

    The includes could (and should) make thins so more reasonable and easier.

*   **Gaps** (since i3 v4.22 - see Debian 12 Bookworm)

    I always liked Gaps but stayed away from **i3-gaps** because
    of its special code base outside standard Package Management
    on *my Distros*.

    I included commentary for Gaps, that can easyly uncommented
    and used.

## Using different i3 Bar Managers and Configs

*   i3bar/i3status
*   polybar
*   polybar themes
*   bumblebee-status

Deployment via ``~/.xsessionrc`` with variables

*   ``JBI3THEME`` - name of Theme
*   ``JBI3VAR`` - name of i3 Bar tech

An Example:

```
export JBI3THEME="nord"
export JBI3VAR="i3standard"
# Example for Line above
# ------------------------------------------------------
# Themes: (Variable $JBI3THEME)
# =======
# nord
# onedark#
# catppuccin
# gruvbox
# dracula
# solarized (no good colortheming)
# ------------------------------------------------------
# i3 Variants: (Variable $JBI3VAR)
# ============
# i3standard (Standard i3)
# polybar (own tryout with vanilla polybar)
# bumblebee (nice python-based status bar helper)
# polybarthemes (Github Repo Polybar Themes modified)
# ------------------------------------------------------

export DESKTOP_SESSION="i3"
# Set i3 configuration via i3 variants with PART(s)
cp ~/.config/i3/config-BASE ~/.config/i3/config
cat ~/.config/i3/config-PART-$JBI3VAR >> ~/.config/i3/config
```

Two Vars set Theme & **Variant** ``JBI3VAR`` for i3 and copy the Variant at
the end of a newly created ``~/.config/i3/config`` on
each **i3 start**.

## Debian and Ubuntu Repositories

Only in the newest **Debian 12 Bookworm** is the newest **i3** Package
available.

The next best Thing - to compiling via Source - are the
Repositories provided by Community: https://i3wm.org/docs/repositories.html

Hint: for Linux Mint you must replace the Codename like *focal* to the apt-line.

    deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe
    # becomes
    deb http://debian.sur5r.net/i3/ focal universe
    # for Linux Mint 20.x


## Getting the theming done

That obviously various per Variant.

The variable ``$theme`` gets created top of the i3 config with ``set $theme $JBI3THEME``

### i3standard

The colors are in ``~/.config/i3status/config-$theme``



### bumblebee

The colors are in ``~/.config/bumblebee-status/themes/$theme-powerline.json``


## Various Topics

Some stuff found on my journey to i3 WM.


### i3 - the classical Window Manager

And no - i am not using the *so called fancy i3-gaps* version ;-)

We need three config Folders for **i3**, **i3blocks** and **i3status**.
These are the core-components for i3. Later we may change the
status Bar using **polybar**.

So copy the Folders to your own ``~/.config/i3*``

You may do these in the CLI or with Midnight Commander - feel free.

### using Flatpak

Flatpaks provide up-to-date Software in many cases. But after such
installation we lack the standard Launcher Support with i3 / Rofi
without providing the ``*.desktop`` file.

Example: Installation of OnlyOffice via Flatpak

``flatpak install flathub org.onlyoffice.desktopeditors``

My Rofi Launcher will miss those Installs without providing a
proper Desktop file. Those Desktop files reside in:

``/var/lib/flatpak/exports/share/applications``

So just create the neccessary link:

``ln -s /var/lib/flatpak/exports/share/applications/org.onlyoffice.desktopeditors.desktop ~/.local/share/applications``



### i3ipc

The **i3 interprocess communication** (or ipc) is the interface i3wm uses
to receive commands from client applications such as i3-msg.

It also features a publish/subscribe mechanism for notifying interested parties of window manager events.

Grabbed by: [Github Repo altdesktop/i3ipc-python](https://github.com/altdesktop/i3ipc-python)

This Python Module ist also required by **autotiling**!

And there are some nice **Example Scripts** available.


### Autotiling with Python Script

Needs package ``python3-i3ipc`` (already included in basic install packages)

Links:

*   [The Linux Cast - Using Autotiling on i3 Makes It Even BETTER](https://www.youtube.com/watch?v=58ARfBt3UP0)

*   [Github Repo - autotiling](https://github.com/nwg-piotr/autotiling)

After configration.sh there should be ``~/.local/bin/autotiling`` (the main.py python script from Repo).


### Emojis

An epic Topic - here ideas from Github user **thingsiplay**:


*   Newer: https://github.com/thingsiplay/emojicherrypick
*   https://github.com/thingsiplay/emojipick

It's a python script for Emojis.

For tool ``notifiy-send`` you may need ``libnotify-bin`` package.
