cd ~/.local/bin
wget https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
chmod u+x nvim.appimage
ln -s nvim.appimage nvim

echo '*******************************************'
echo 'Neovim (Stable) as AppImage in ~/.local/bin'
echo '*******************************************'

git clone https://github.com/joebrandes/lazyvim-config ~/.config/nvim

echo '***********************************************'
echo 'Neovim (Blanko) with my Lazyvim Config deployed'
echo '***********************************************'
