# Podman (with external sources - here: Alvistack Version 4.8; Debian 12 Bookworm: Version 4.3.1)
# ======

# https://computingforgeeks.com/how-to-install-podman-on-debian-12-bookworm/?expand_article=1
# https://build.opensuse.org/package/show/home%3Aalvistack/containers-podman-4.8.0
# https://software.opensuse.org/package/podman

# Get Alvistack Key
# -----------------
# Get $VERSION_ID - Problem with Debian Derivates like LMDE 6
source /etc/os-release
wget http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_$VERSION_ID/Release.key -O alvistack_key
# on LMDE 6
# wget http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_12/Release.key -O alvistack_key
cat alvistack_key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/alvistack.gpg  >/dev/null

# Add Repo
# --------
echo "deb http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_$VERSION_ID/ /" | sudo tee  /etc/apt/sources.list.d/alvistack.list
# on LMDE 6 no 12 for Debian_12 VERSION_ID
# echo "deb http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_12/ /" | sudo tee  /etc/apt/sources.list.d/alvistack.list

sudo nala update && sudo nala upgrade -y
# LMDE6 remark: immense updates for python stuff on LMDE 6
sudo nala install curl gpg gnupg2 software-properties-common apt-transport-https lsb-release ca-certificates -y


# Install Podman
# --------------
# Original line from tutorial:
# sudo apt install podman python3-podman-compose
# Improved line for non rootless error

sudo nala install podman python3-podman-compose uidmap slirp4netns

# Problem: will not run rootless
# ------------------------------
# https://forum.armbian.com/topic/29244-podman-rootless-fails-with-newuidmap-executable-file-not-found-in-path/
# https://github.com/containers/podman/issues/2211
# sudo apt install podman uidmap slirp4netns

# Podman Test
# -----------
podman info
podman run docker.io/library/hello-world



# Distrobox
# =========

# https://github.com/89luca89/distrobox
# https://distrobox.it/#distrobox

curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local

# Upgrading:
# ----------
# Just run the curl or wget command again.

# Uninstall:
# ----------
# curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/uninstall | sh -s -- --prefix ~/.local

# Distrobox Test
# --------------
distrobox create -i alpine:latest
distrobox enter alpine-latest
distrobox ls
distrobox stop alpine-latest
