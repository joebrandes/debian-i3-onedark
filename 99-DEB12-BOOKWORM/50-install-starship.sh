# Starship install
curl -sS https://starship.rs/install.sh | sh

echo '******************'
echo 'Starship installed'
echo '******************'

# Starship
cp ../50-starship/.config/starship.toml ~/.config/
echo '********************'
echo 'Starship.toml copied'
echo '********************'

# echo 'eval "$(starship init bash)"' >> ~/.bashrc
# echo '*********************************'
# echo 'Starship line appended in .bashrc'
# echo '*********************************'

echo '************************************************'
echo 'My .bash_aliases provides eval line for Starship'
echo '************************************************'

# Reboot
echo '************************'
echo 'Please consider a REBOOT'
echo '************************'
