Table of Contents

- [Demoinstall Debian 12 for Seminars and Lab Environments](#demoinstall-debian-12-for-seminars-and-lab-environments)
  - [Basic Install](#basic-install)
  - [Order in Dash of Gnome](#order-in-dash-of-gnome)
  - [Workspaces](#workspaces)
- [Installation Chapters](#installation-chapters)

An Impression to start of with: (Theme: Nord)

![Screenshot Debian 12 with PopOS Tiling WM and Nemo, Ranger, Kitty and Rofi](./PICS/debian12-with-popos-shell.png)

And of we go...

# Demoinstall Debian 12 for Seminars and Lab Environments

Use cases: Create various Templates for Virtualization Environments like ProxmoxVE, KVM/qemu or Hyper-V.

```
There is a Hyper-V-Template available on my Trainer OneDrive 4 Trainees
```

Or go and take these explanations combined with Git Repo for individual new Installations.

## Basic Install

+   User: **seminar** / Pass: **seminar** (with sudo)
+   Graphical Install with
    60 GB Virtual HDD (LVM Support without Partitions)
    or 127 GB Standard Size for Hyper-V
    and DE Gnome - active with SSH
+   Wayland (and Pipewire) modern DE Infrastructure **AND X.org - see: Rofi and Co**
+   Dark Gnome Theme (Standard)
+   Gnome Terminal - Tango Dark later Nord with Font Monospace 16 or Meslo/FiraCode
+   Title Bar - minimize/maximize Buttons via Tweak Tool
+   Theming: Nord (see www.nordtheme.com)

## Order in Dash of Gnome

We want put the keyboard standards `Super` + `#` to good use:

1.  Tool: `firefox-esr` - Firefox (Browser)
2.  Tool: `nautilus` - Dateien / Files
3.  Tool: `gnome-terminal` - Terminal
4.  Tool: `gnome-text-editor` - Texteditor (Coding - it is **not** `gedit` by the way)

So as always - on my systems - use `Super` + `3` for your terminal.

And as always: `Super` is (mostly ;-) your `Win` key.

Later we want those standard applications to go to our standard **Workspaces** automatically!

## Workspaces

Get ourselves a start with **4 static workspaces** - name will be provided later on via Gnome Extensions.

1.  Browser
2.  Files
3.  Terminal
4.  Coding

Configuration via Gnome Settings - Multitasking - Workspaces. Test via `Super` key shows 4 Workspaces on top.

I use a total of 9 Workspaces: 5 - VMs; 6 - Office; 7 - Email; 8 - Graphics; 9 - System

---

    REMINDER:

    Use Snapshot - or whatever your Tech or Virtualization Environment calls it - to freeze your
    Installation steps!

---

# Installation Chapters

More Chapters with further Installations and Improvements:

*   [NALA - Package Management improvement to apt Tools](10-NALA.md)
*   [PopOS Shell from System76 - Tiling Windows with Gnome!](20-PopShell.md)
*   [Software Completion - more Tools to have the Feel of Tiling Window Manager](30-Software-Completion.md)
