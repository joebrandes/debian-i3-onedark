Table of contents

- [Software Completion - mainly for the terminal](#software-completion---mainly-for-the-terminal)
- [Kitty - Terminal of Choice](#kitty---terminal-of-choice)
- [Neovim - Terminal editor of Choice](#neovim---terminal-editor-of-choice)
- [LSD - an alternitive to ls](#lsd---an-alternitive-to-ls)
- [Nemo as Alternative to Files/Nautilus](#nemo-as-alternative-to-filesnautilus)
- [Rofi - make Startmenu via Shortcuts](#rofi---make-startmenu-via-shortcuts)
- [Ranger - filemanager for the terminal](#ranger---filemanager-for-the-terminal)


I need a bit more stuff to feel fine on my Systems. And again:
i want to forget i am on a Desktop Environment - a Gnome DE none the less!

    Terminal Tools like **Rofi** do not work with **Wayland** Composing!
    You need the **old/proper X-Server**!

# Software Completion - mainly for the terminal

I start with my ideas of my **i3** Window Manager environment and will
break down the supplimentary Software i need for my console based work.

I scrape everything **i3** related or extra **X-server** because of my
already full functioning Gnome Environment with **Wayland** and/or **X**.

And i will leave out everything already installed like `firefox-esr`
or `evolution` or would be neccessary for i3-Panels like `gsimplecal`.

```
#!/bin/bash
# Update your system
sudo apt update && sudo apt upgrade -y

# Prepare local folders
mkdir ~/bin
mkdir -p ~/.local/bin
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/.local/share/applications
mkdir ~/.icons
mkdir ~/tmp
mkdir ~/opt

# Software installs
sudo apt install gedit vlc neofetch command-not-found atool lsd \
    zsh rxvt-unicode rofi ranger mc vim git make tmux curl wget cifs-utils \
    python3-pip python3-dev python3-pygit2 python3-venv python3-psutil \
    fzf fd-find ripgrep bat atool python3-pygments python3-pil \
    nemo fonts-noto fonts-font-awesome papirus-icon-theme fontforge libharfbuzz-bin \
    xclip highlight htop figlet viewnior ueberzug \
    zathura zathura-pdf-poppler zathura-ps

curl -sS https://starship.rs/install.sh | sh

echo ' '
echo '*************'
echo 'Please REBOOT and you could copy the config stuff one-to-one'
echo '*************'

```
A few programs i will deploy differently because of the newer version
delivered by those install procedures.

*   kitty
*   neovim
*   Nautilus (Dateien) Alternative: Nemo

# Kitty - Terminal of Choice

Just follow the Developers path to installation:

*   [Kitty website Developer Kovid Goyal](https://sw.kovidgoyal.net/kitty/)
*   [Kitty on Github](https://github.com/kovidgoyal/kitty)


You can install pre-built binaries of kitty if you are on macOS or Linux using the following simple command:

`curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin`

The binaries will be installed in the standard location for your OS, /Applications/kitty.app on macOS
and `~/.local/kitty.app` on Linux.

To be clear:

```
❯ ls -Al $(which kitty)
lrwxrwxrwx 1 seminar seminar 40 23. Feb 20:53 /home/seminar/.local/bin/kitty -> /home/seminar/.local/kitty.app/bin/kitty
```

Integration of **kitty.app** in DE: https://sw.kovidgoyal.net/kitty/binary/

```
# Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in
# your system-wide PATH)
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
# Place the kitty.desktop file somewhere it can be found by the OS
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
# If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
# Update the paths to the kitty and its icon in the kitty.desktop file(s)
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
```



The installer only touches files in that directory. To update kitty, simply re-run the command.

For **kitty** theming use the prepared files in folder (you may alread use them / copied them):

*   `40-kitty/.config/kitty/*` - Kittey Themes / Standard-Theme-Name: `kitty.conf`
*   `40-kitty/.local/share/nemo/actions/open_in_kitty.nemo_action` - speaks for itself

**Gnome Terminal**

If you like to stay and use the Standardterminal **Gnome Terminal** feel free
to change to **Theme Nord** via https://github.com/nordtheme/gnome-terminal.

You should then install `sudo apt install uuid-runtime dconf-editor` to perform the tasks
for theming Gnome Terminal.

# Neovim - Terminal editor of Choice

Neovim in Debian 12 only in version **0.7.2-7** while i am *recording* this.
The newest version is **0.9.5**!

*   [Neovim Website](https://neovim.io/)
*   [Neovim Github](https://github.com/neovim/neovim)

We will use the Pre-built archives for a system-wide **Neovim**:

The Releases page provides pre-built binaries for Linux systems.

```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz
```

After this step add this to `~/.bashrc`: (already in my `.bash_aliases`!)

`export PATH="$PATH:/opt/nvim-linux64/bin"`

Without any further Configuration run `:checkhealth`

I would go no further ;-) - but if you want to...

You should think twice about the needed Neovim/Python3 Environment install:

`sudo apt install python3-neovim` is a problem because it install **old Neovim** from Debian Repos!

But that at least solves Neovim *problems* with Clipboard package `xclip` and other stuff.

One of possible Starts with Neovim ist LazyVim (as Plugin Manager)
or complete Environment like:

`git clone https://gitlab.com/joebrandes/lazyvim-config ~/.config/nvim`

Latest test in February 2024 checked out - system functioning but lots and lots of
messages while using because of all the missing **LSP** and **Scripting Env** stuff.


# LSD - an alternitive to ls

Simple Installation of the newest version including the support for
color management via `.config/lsd/colors.yaml` combined with `.config/lsd/config.yaml`.

Download DEB (without any dependencies!): https://github.com/lsd-rs/lsd

And then: `dpkg -i lsd_1.0.0_arm64.deb` (or whatever newer version)

For Coloring: https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg

# Nemo as Alternative to Files/Nautilus

Works nicely with `papirus-icon-theme` and those colors and has a bit more Tech onBoard.

[Papirus Folder Color changer](https://github.com/PapirusDevelopmentTeam/papirus-folders)

Then change standard to **nordic**: `papirus-folder -C nordic`

You can get even more Nord-Colors with https://github.com/Adapta-Projects/Papirus-Nord.


# Rofi - make Startmenu via Shortcuts

There are all neccessary Dotfiles available in the underlying Chapters
for these tools.

Folders:

*   `70-rofi/.config/rofi` - config file for Rofi
*   `70-rofi/.local/share/rofi/themes` - theming for Rofi - or `themes` as subfolder in `.config/rofi`

Shorcut for Rofi: `rofi -show drun -theme nord` with keys `Super` + `d` for me.

Or one of the longer versions provieded as examples in the **i3 configs**.

# Ranger - filemanager for the terminal

Again: all the configuration is available in main project files.

Folder: `7-ranger/.config/ranger/..`

Example: For image previews in **ranger** set configuration in `rc.conf` to `set preview_images_method ueberzug` (or kitty, if problems occur).


