Table of Contents

- [Nala - Software Tool / Package Management](#nala---software-tool--package-management)
- [Short Rekap up front in case of TLDR](#short-rekap-up-front-in-case-of-tldr)
- [Nala Infos and Repos](#nala-infos-and-repos)
- [Nala installation](#nala-installation)
- [Nala use, configuration and Bash optimization](#nala-use-configuration-and-bash-optimization)

Please remember to make `nala` your first choice of Package Tool (see Bash Optimization).

In all Installation Code in next Install Chapters i will write the `apt` syntax to be
free of nala in case neccessary.

# Nala - Software Tool / Package Management

We want improved Debian Package Management with **Nala** from the **Volian** Developers.

*   [https://gitlab.com/volian/nala ](https://gitlab.com/volian/nala )

*   [Installation Wiki Page Volian Nala](https://gitlab.com/volian/nala/-/wikis/Installation)

*   [Chris Titus Tech article nala](https://christitus.com/stop-using-apt/)



A ``apt show nala`` in Debian 12 shows version 0.12.2 which is ab bit old in 2024.

# Short Rekap up front in case of TLDR

*   [Download Volian Archive Repo](https://gitlab.com/volian/volian-archive/uploads/d00e44faaf2cc8aad526ca520165a0af/volian-archive-nala_0.2.0_all.deb)
*   [Download Volian Archive Repo Keyring](https://gitlab.com/volian/volian-archive/uploads/d9473098bc12525687dc9aca43d50159/volian-archive-keyring_0.2.0_all.deb)
*   `sudo apt install ./volian-archive-*.deb`
*   `sudo apt update`
*   `sudo apt show nala`
*   `sudo apt install nala`
*   `sudo nala fetch` ... `sudo nala ...`
*   Optimize `.bashrc` for user and root to type any CLI command
*   Tip: use the optimized `.bash_aliases` from Config-Examples Folder! There may
    be some messages that will go away later on!

More Explanations in following Chapters.


# Nala Infos and Repos

Downloads for needed Repoinfos and installs:

*   [Volian Archive Repos/Releases](https://gitlab.com/volian/volian-archive/-/releases)

```
volian-archive-nala_0.2.0_all.deb
SHA256 bfffe3ab246526b983e1b303e8ae03e6c369a6dbc40ad4f71529f98c35355755

volian-archive-keyring_0.2.0_all.deb
SHA256 73395b83d469f1da8b503c304c65eb757647cb791e566dcaca4be2b860689f42
```

Downloads look like:

```
seminar@deb12demo:~/Downloads$ ls -Al
insgesamt 12
-rw-r--r-- 1 seminar seminar 4180 18. Feb 13:11 volian-archive-keyring_0.2.0_all.deb
-rw-r--r-- 1 seminar seminar 2392 18. Feb 13:11 volian-archive-nala_0.2.0_all.deb
```

Integrate the Repo infos:

```
sudo apt install ./volian-archive-*.deb
```

We get

```
seminar@deb12demo:~/Downloads$ ls -Al /etc/apt/sources.list.d/
insgesamt 4
-rw-r--r-- 1 root root 150  7. Sep 2022  volian-archive-scar-unstable.sources
seminar@deb12demo:~/Downloads$ cat /etc/apt/sources.list.d/volian-archive-scar-unstable.sources
Types: deb deb-src
URIs: https://deb.volian.org/volian/
Suites: scar
Components: main
Signed-By: /usr/share/keyrings/volian-archive-scar-unstable.gpg
```

# Nala installation

Update your repos and check that your repo refresh show infos for the newer **nala** software:

```
sudo apt update
seminar@deb12demo:~$ apt show nala
Package: nala
Version: 0.15.1
Priority: optional
Section: admin
Maintainer: Volian Developers <volian-devel@volian.org>
Installed-Size: 696 kB
Depends: python3-anyio, python3-httpx, python3-pexpect, python3-rich, python3-tomli, python3-typer, python3-typing-extensions, python3:any, apt, python3-apt, python3-debian
Recommends: python3-socksio
Homepage: https://gitlab.com/volian/nala
Download-Size: 142 kB
APT-Sources: https://deb.volian.org/volian scar/main amd64 Packages
Description: Commandline frontend for the APT package manager
 Nala is a frontend for the APT package manager. It has a lot
 of the same functionality, but formats the output to be more
 human readable. Also implements a history function to see past
 transactions and undo/redo them. Much like Fedora's dnf history.

N: Es gibt 3 zusätzliche Einträge. Bitte verwenden Sie die Option »-a«, um sie anzuzeigen.
```

Let's install **nala** and check the version:

```
seminar@deb12demo:~$ sudo apt install nala
Paketlisten werden gelesen… Fertig
Abhängigkeitsbaum wird aufgebaut… Fertig
Statusinformationen werden eingelesen… Fertig
Die folgenden zusätzlichen Pakete werden installiert:
  python3-anyio python3-click python3-colorama python3-h11 python3-httpcore python3-httpx python3-markdown-it
  python3-mdurl python3-pexpect python3-ptyprocess python3-pygments python3-rfc3986 python3-rich python3-sniffio
  python3-socksio python3-tomli python3-typer python3-typing-extensions
Vorgeschlagene Pakete:
  python-pexpect-doc python-pygments-doc ttf-bitstream-vera python-typer-doc
Die folgenden NEUEN Pakete werden installiert:
  nala python3-anyio python3-click python3-colorama python3-h11 python3-httpcore python3-httpx python3-markdown-it
  python3-mdurl python3-pexpect python3-ptyprocess python3-pygments python3-rfc3986 python3-rich python3-sniffio
  python3-socksio python3-tomli python3-typer python3-typing-extensions
0 aktualisiert, 19 neu installiert, 0 zu entfernen und 0 nicht aktualisiert.
Es müssen 1.745 kB an Archiven heruntergeladen werden.
Nach dieser Operation werden 9.151 kB Plattenplatz zusätzlich benutzt.
Möchten Sie fortfahren? [J/n]

...

seminar@deb12demo:~$ nala --version
nala 0.15.1
```

# Nala use, configuration and Bash optimization

Let's optimize und use **nala**:

```
sudo nala fetch
Debian-Spiegelserver werden abgerufen…
╭─  Selected Mirrors ─────────────────────────────────────────────────────────────────────╮
│                                                                                         │
│   Index    Mirror                              Score                                    │
│       3    http://ftp.ch.debian.org/debian/    12 ms                                    │
│       5    https://artfiles.org/debian/        12 ms                                    │
│                                                                                         │
│   Punktzahl gibt an, wie viele Millisekunden das Herunterladen der Release-Datei dauert │
╰─────────────────────────────────────────────────────────────────────────────────────────╯
Sind diese Spiegelserver in Ordnung? [J/n] J
Quellen wurden in /etc/apt/sources.list.d/nala-sources.list geschrieben

seminar@deb12demo:~$ sudo nala update
Keine Änderung: http://deb.debian.org/debian bookworm InRelease
Keine Änderung: http://security.debian.org/debian-security bookworm-security InRelease
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm InRelease [151 KB]
Keine Änderung: http://deb.debian.org/debian bookworm-updates InRelease
Aktualisiert:   https://artfiles.org/debian bookworm InRelease [151 KB]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main amd64 Packages [8.8 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main amd64 Packages [8.8 MB]
Keine Änderung: https://deb.volian.org/volian scar InRelease
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main Translation-en [6.1 MB]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main Translation-de [1.7 MB]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main Translation-de_DE [830 Bytes]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main amd64 DEP-11 Metadata [4.5 MB]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main DEP-11 48x48 Icons [3.6 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main Translation-de [1.7 MB]
Aktualisiert:   http://ftp.ch.debian.org/debian bookworm/main DEP-11 64x64 Icons [7.3 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main Translation-de_DE [830 Bytes]
Aktualisiert:   https://artfiles.org/debian bookworm/main Translation-en [6.1 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main amd64 DEP-11 Metadata [4.5 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main DEP-11 48x48 Icons [3.6 MB]
Aktualisiert:   https://artfiles.org/debian bookworm/main DEP-11 64x64 Icons [7.3 MB]
Geholt 64.4 MB in 7s (9.2 MB/s)
Alle Pakete sind aktuell.
```

**Optimize Shell Environment Bash**

[Chris Titus:] Use **nala** or **apt** in CLI - add the following to your `~/.bashrc` AND `/root/.bashrc` file:

```
apt() {
  command nala "$@"
}
sudo() {
  if [ "$1" = "apt" ]; then
    shift
    command sudo nala "$@"
  else
    command sudo "$@"
  fi
}
```

From here you can install programs with apt or nala command and it will always work perfectly!

Because i am using most of my Machines directly (e.g. Spice VirtViewer) or via SSH i prefer a proper
reboot to get the system fresh and running for further tests.


