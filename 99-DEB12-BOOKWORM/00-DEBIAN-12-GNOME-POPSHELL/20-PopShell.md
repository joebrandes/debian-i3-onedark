Table of Contents
- [PopOs Shell](#popos-shell)
- [Pop OS Shell Installation](#pop-os-shell-installation)
- [Gnome Extensions to maximize Look \& Feel](#gnome-extensions-to-maximize-look--feel)

We want to bring proper tiling to the Gnome Environment and thank System76 for that.


# PopOs Shell

The main thing for using a Desktop Environments or Windows Manager
for me is always be supported by those Techs.

So i find myself mostly in Window Managers of various descend.

Using Debian brings the Gnome Desktop and nothing fun in general
regarding managing Windows (looking for tiling) or automatic Workspace handlings.

Intro: Cosmic Desktop Development by System76.

I use the **PopOS! Shell** which will help with the Management of Windows
and brings the *feeling* of a tiling Window Manager.

[PopOS Shell on Github](https://github.com/pop-os/shell)

At time of this documentation the **Gnome Desktop for Debian 12 uses Version 43.9**,
which will be of importance for the installation via Git Repo.

# Pop OS Shell Installation

We must have at least the following requirements:

*   GNOME Shell 3.36 (OK)
*   TypeScript 3.8 - `sudo apt install nodejs npm node-typescript`
    (we will very likely need the nodejs typescript tech in later projects)
*   GNU Make - `sudo apt install make`

We use the TypeScript install with Git provide Code for PopOS Shell:

*   `sudo apt install git`
*   `mkdir ~/popos-shell && cd ~/popos-shell`
*   `git clone https://github.com/pop-os/shell.git`
*   `cd shell`
*   `git branch` # ups: master_mantic
*   `git checkout master_jammy`

Please check before install:

```
seminar@deb12demo:~/popos-shell/shell$ git branch
* master_jammy
  master_mantic
```

Let's do the install finally:

*   `make local-install`

The Gnome Desktop will go into Nirvana... ;-) just go to a Terminal and restart the Machine.

# Gnome Extensions to maximize Look & Feel

Provide the Gnome Extensions via Browser: [Extensions for Browser](https://extensions.gnome.org/).

This now let's us look and manipulate our local Extension installs. So: activate the **Pop Shell**.
We get a special Icon in the top Gnome bar!

Other Extensions i like and use:

*   AppIndicator and KStatusNotifierItem Support (3v1no) - will be needed later on e.g. Docker Desktop
*   Applications Menu (mnuellner) - a clickable Start Menu (for those who want's that stuff ;-)
*   Auto Move Windows (fmuellner) - Windows automatically on our Workspace of Choice
*   Dash to Panel (wild stuff - we nearly go KDE Plasma) - do not do Animations!
*   Extension List (grrot) - see all Gnome Extensions
*   Improved Workspace Indicator - see your Workspaces like in i3 (or sway)
*   Launch New Instance (fmuellner) - new window/instance for programs
*   Places Status Indicator (fmuellner) - find your Places
*   **Pop Shell - the main reason to get my hands dirty with Gnome ;-)**
*   Removable Drive Menu (fmuellner) - for USB or Network Resources...
*   TopHat (fflewddur) - resource monitor stuff / nice looking - needs `gir1.2-gtop-2.0` package
*   Window List (fmuellner) - Window list at the bottom of screen with workspaces


