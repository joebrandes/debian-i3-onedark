! ------------------------------------------------------------------------------
! Colour Configuration
! ------------------------------------------------------------------------------
! Copyright (c) 2016-present Arctic Ice Studio <development@arcticicestudio.com>
! Copyright (c) 2016-present Sven Greb <code@svengreb.de>

! Project:    Nord XResources
! Version:    0.1.0
! Repository: https://github.com/arcticicestudio/nord-xresources
! License:    MIT


! Copyright (c) 2016-present Sven Greb <development@svengreb.de>
! This source code is licensed under the MIT license found in the license file.

#define nord0 #2E3440
#define nord1 #3B4252
#define nord2 #434C5E
#define nord3 #4C566A
#define nord4 #D8DEE9
#define nord5 #E5E9F0
#define nord6 #ECEFF4
#define nord7 #8FBCBB
#define nord8 #88C0D0
#define nord9 #81A1C1
#define nord10 #5E81AC
#define nord11 #BF616A
#define nord12 #D08770
#define nord13 #EBCB8B
#define nord14 #A3BE8C
#define nord15 #B48EAD

*.foreground:   nord4
*.background:   nord0
*.cursorColor:  nord4
*fading: 35
*fadeColor: nord3

*.color0: nord1
*.color1: nord11
*.color2: nord14
*.color3: nord13
*.color4: nord9
*.color5: nord15
*.color6: nord8
*.color7: nord5
*.color8: nord3
*.color9: nord11
*.color10: nord14
*.color11: nord13
*.color12: nord9
*.color13: nord15
*.color14: nord7
*.color15: nord6

!------------------------------------------------------------------------------
! Font configuration
! ------------------------------------------------------------------------------

!URxvt*font:		xft:M+ 1mn:regular:size=12
!URxvt*boldFont:		xft:M+ 1mn:bold:size=12
!URxvt*italicFont:	xft:M+ 1mn:italic:size=12
!URxvt*boldItalicFont:	xft:M+ 1mn:bold italic:size=12

URxvt*font:		xft:MesloLGS NF:regular:size=12
URxvt*boldFont:		xft:MesloLGS NF:bold:size=12
URxvt*italicFont:	xft:MesloLGS NF:italic:size=12
URxvt*boldItalicFont:	xft:MesloLGS NF:bold italic:size=12

! Terminal Colors
URxvt*vt100.background:     #303446
URxvt*vt100.foreground:     #C6D0F5
URxvt*cursorColor:          #F2D5CF
!
! Nord Theme
!
URxvt*color0:   #51576D
URxvt*color8:   #626880
URxvt*color1:   #E78284
URxvt*color9:   #E78284
URxvt*color2:   #A6D189
URxvt*color10:  #A6D189
URxvt*color3:   #E5C890
URxvt*color11:  #E5C890
URxvt*color4:   #8CAAEE
URxvt*color12:  #8CAAEE
URxvt*color5:   #F4B8E4
URxvt*color13:  #F4B8E4
URxvt*color6:   #81C8BE
URxvt*color14:  #81C8BE
URxvt*color7:   #B5BFE2
URxvt*color15:  #A5ADCE





! Xterm
XTerm*vt100.faceName:   xft:MesloLGS NF:size=11:antialias=true:style=Regular
XTerm*vt100.boldFont:   xft:MesloLGS NF:size=11:antialias=true:style=Bold

! Terminal Colors
XTerm*vt100.background:     #303446
XTerm*vt100.foreground:     #C6D0F5
XTerm*cursorColor:          #F2D5CF

! Color Scheme
! 0-7:  Black, Red, Green, Yellow, Blue, Purple, Cyan, White
! 8-15: Bright Black, Bright Red, ...
!XTerm*color0:               #212121
!XTerm*color8:               #7f7f7f
!XTerm*color1:               #A00000
!XTerm*color9:               #FF0000
!XTerm*color2:               #00A000
!XTerm*color10:              #00FF00
!XTerm*color3:               #A0A000
!XTerm*color11:              #FFFF00
!XTerm*color4:               #0000A0
!XTerm*color12:              #5C5CFF
!XTerm*color5:               #A000A0
!XTerm*color13:              #FF00FF
!XTerm*color6:               #00A0A0
!XTerm*color14:              #00FFFF
!XTerm*color7:               #C0C0C0
!XTerm*color15:              #ffffff

! Nord Theme
!
XTerm*color0:   #51576D
XTerm*color8:   #626880
XTerm*color1:   #E78284
XTerm*color9:   #E78284
XTerm*color2:   #A6D189
XTerm*color10:  #A6D189
XTerm*color3:   #E5C890
XTerm*color11:  #E5C890
XTerm*color4:   #8CAAEE
XTerm*color12:  #8CAAEE
XTerm*color5:   #F4B8E4
XTerm*color13:  #F4B8E4
XTerm*color6:   #81C8BE
XTerm*color14:  #81C8BE
XTerm*color7:   #B5BFE2
XTerm*color15:  #A5ADCE


! ------------------------------------------------------------------------------
! Xft Font Configuration
! ------------------------------------------------------------------------------

Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintslight
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb

! ------------------------------------------------------------------------------
! URxvt configs
! ------------------------------------------------------------------------------

! font spacing
URxvt*letterSpace:		0
URxvt.lineSpace:		0

! general settings
URxvt*saveline:			15000
URxvt*termName:			rxvt-256color
URxvt*iso14755:			false
URxvt*urgentOnBell:		true

! appearance
URxvt*depth:			24
URxvt*scrollBar:		false
URxvt*scrollBar_right:	false
URxvt*internalBorder:	24
URxvt*externalBorder:	0
URxvt.geometry:			84x22

! perl extensions
URxvt.perl-ext-common:	default,clipboard,url-select,keyboard-select

! macros for clipboard and selection
URxvt.copyCommand:		xclip -i -selection clipboard
URxvt.pasteCommand:		xclip -o -selection clipboard
URxvt.keysym.M-c:		perl:clipboard:copy
URxvt.keysym.M-v:		perl:clipboard:paste
URxvt.keysym.M-C-v:		perl:clipboard:paste_escaped
URxvt.keysym.M-Escape:	perl:keyboard-select:activate
URxvt.keysym.M-s:		perl:keyboard-select:search
URxvt.keysym.M-u:		perl:url-select:select_next
URxvt.urlLauncher:		firefox
URxvt.underlineURLs:	true
URxvt.urlButton:		1

! scroll one line
URxvt.keysym.Shift-Up:		command:\033]720;1\007
URxvt.keysym.Shift-Down:	command:\033]721;1\007

! control arrow
URxvt.keysym.Control-Up:	\033[1;5A
URxvt.keysym.Control-Down:	\033[1;5B
URxvt.keysym.Control-Right:	\033[1;5C
URxvt.keysym.Control-Left:	\033[1;5D

! ------------------------------------------------------------------------------
! Rofi configs
! ------------------------------------------------------------------------------

!rofi.color-enabled: true
!rofi.color-window: #2e3440, #2e3440, #2e3440
!rofi.color-normal: #2e3440, #d8dee9, #2e3440, #2e3440, #bf616a
!rofi.color-active: #2e3440, #b48ead, #2e3440, #2e3440, #93e5cc
!rofi.color-urgent: #2e3440, #ebcb8b, #2e3440, #2e3440, #ebcb8b
!rofi.modi: run,drun,window

! ------------------------------------------------------------------------------
! Dmenu configs
! ------------------------------------------------------------------------------

!dmenu.selforeground:	    #d8dee9
!dmenu.background:	        #2e3440
!dmenu.selbackground:	    #bf616a
!dmenu.foreground:	        #d8dee9

xclock*width:            500
xclock*height:           500
!xclock*background:       #2E3440
xclock*background:       nord0
!xclock*Foreground:       #88C0D0
xclock*Foreground:       nord8
xclock*padding:          30
xclock*hourColor:        nord5
