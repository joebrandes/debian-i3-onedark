# ============================================================================
# ~/.bash_aliases
# will be automatically included in .bashrc
# ============================================================================


# JoeB Stuff

# interchange CLI commands nala and apt [Chris Titus lines]

apt() {
  command nala "$@"
}
sudo() {
  if [ "$1" = "apt" ]; then
    shift
    command sudo nala "$@"
  else
    command sudo "$@"
  fi
}


# use Neovim Pre-Built Archive Releases
export PATH="$PATH:/opt/nvim-linux64/bin"

# FZF - RIPGREP
# ============================================================================
# {{{

# https://github.com/junegunn/fzf
# fzf environment and exports:
# Original Debian:
. /usr/share/doc/fzf/examples/key-bindings.bash
# . /usr/share/doc/fzf/examples/completion.bash
# . $HOME/.config/fzf/examples/key-bindings.bash
# . $HOME/.config/fzf/examples/completion.bash
# not good to put preview to FZF_DEFAULT_OPTS - look down to other opts
export FZF_DEFAULT_OPTS='--height 75% --border'
# use ripgrep with FZF instead of find
# export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'
# more commands:
export FZF_ALT_C_COMMAND='find . -type d'
# Print tree structure in the preview window
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"
# Preview file content using bat/batcat (https://github.com/sharkdp/bat)
export FZF_CTRL_T_OPTS="
  --preview 'batcat --style numbers,changes --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# configuration for ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# ============================================================================
# CDD - Change Directory with Dialogue
alias cddhome="cd ~ && cd \$(find . -type d | fzf)"
alias cddprojects="cd /var/www/html && cd \$(find * -type d | fzf)"
alias nf="nvim \$(rg --files --hidden -g !.git | fzf)"
alias nd="nvim \$(find . -type d | fzf)"

# ============================================================================
# }}}


# Exports
# ============================================================================
# {{{

# fixing Exports for RANGER standard Editor vim or nvim
export VISUAL=vim
export EDITOR=vim

# ============================================================================
# }}}


# Starship
# ============================================================================
# {{{

[ -x /usr/local/bin/starship ] && eval "$(starship init bash)"

# ============================================================================
# }}}
