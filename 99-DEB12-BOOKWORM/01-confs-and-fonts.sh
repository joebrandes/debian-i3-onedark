#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

echo ' *******************************************'
echo ' No Actions for Bash, Zsh, Nvim or Starship!'
echo ' *******************************************'


# Prepare local folder
mkdir ~/bin
mkdir -p ~/.local/bin
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir ~/.icons
mkdir ~/tmp
mkdir ~/opt

echo '****************'
echo 'Prepared Folders'

# CP i3 stuff
cp -R ../00-WM-i3-i3block-i3status/.config/i3* ~/.config/
cp ../00-WM-i3-i3block-i3status/.local/bin/* ~/.local/bin/
echo '*****************************************'
echo 'i3 stuff copied (incl. manual autotiling)'

# CP Bumblebee-Status stuff
cp -a ../05-bumblebee-status/.config/bumblebee-status ~/.config/
echo '*****************************'
echo 'Bumblebee-Status stuff copied'

# CP Polybar stuff
cp -R ../05-polybar/.config/polybar ~/.config/
echo '********************'
echo 'Polybar stuff copied'
# CP Polybar Themes stuff
cp -R ../05-polybar/.config/polybar-themes ~/.config/
echo '***************************'
echo 'Polybar Themes stuff copied'
cp -R ../05-polybar/.config/polybar-testing ~/.config/
echo '***************************'
echo 'Polybar Testing stuff copied'


# CP picom and compton conf
cp ../10-picom/.config/picom.conf ~/.config/
# cp 10-compton/.config/compton.conf ~/.config/
echo '*****************************************'
echo 'Picom (Compton is deprecated) conf copied'

# Xorg
cp ../10-Xorg-Resources/.Xresources* ~
cp ../10-Xorg-Resources/.xsession* ~
cp ../10-Xorg-Resources/.xinit* ~
echo '*******************************************************'
echo 'Xorg Resources (.Xresources, .xsession, .xinit) copied'

# Existing .bashrc backup
mv ~/.bashrc ~/.bashrc-$(date -I)

# Bash
cp ../20-bash/.bashrc ~
cp ../20-bash/.bash_aliases ~
echo '********************************'
echo '.bashrc and .bash_aliases copied'


# Kitty
cp -R ../40-kitty/.config/kitty/ ~/.config/
cp -R ../40-kitty/.local/share/* ~/.local/share/
echo '****************************'
echo 'Kitty conf and themes copied'

# Urxvt
cp -R ../40-urxvt/.urxvt/ ~
echo '***************************************'
echo 'Urxvt stuff for clipboard and co copied'

# FZF - RIPGREP
cp -R ../50-fzf/.config/fzf ~/.config/
cp ../50-fzf/.ripgreprc ~
echo '**********************************'
echo 'FZF examples und RIPGREP RC copied'

# Vim
cp ../60-vim/.vimrc ~
cp -R ../60-vim/.vim ~
echo '****************'
echo 'Vim stuff copied'

# Bat
mkdir -p ~/.config/bat 2>/dev/null
cp ../70-bat-batcat/.config/bat/config ~/.config/bat/
echo '****************'
echo 'Bat stuff copied'

# MC
mkdir -p ~/.local/share/mc/skins 2>/dev/null
cp -R ../70-mc/.local/share/mc/skins/*.ini ~/.local/share/mc/skins
# cp -R ../70-mc/.config/mc ~/.config/
echo '***************'
echo 'MC skins copied'

# Ranger
cp -R ../70-ranger/.config/ranger ~/.config/
echo '*******************'
echo 'Ranger stuff copied'

# Rofi
cp -R ../70-rofi/.config/rofi ~/.config/
cp -R ../70-rofi/.local/share/rofi ~/.local/share/
echo '***************************'
echo 'Rofi conf and themes copied'

# Pictures
mkdir ~/pictures
cp -R ../90-PICS/* ~/pictures
echo '***************'
echo 'Pictures copied'

# Fonts - no folder .fonts in naked system
fonts="$HOME/.fonts"
localsharefonts="$HOME/.local/share/fonts"
[ ! -d "$fonts" ] && mkdir -p "$fonts"
[ ! -d "$localsharefonts" ] && mkdir -p "$localsharefonts"

cp -R ../90-FONTS/.fonts/* ~/.fonts
cp -R ../90-FONTS/.local/share/fonts/* ~/.local/share/fonts/
echo '****************************************************'
echo 'MyFonts for ~/.fonts and ~/.local/share/fonts copied'
