# Debian 12 Bookworm - Quick install/prepair

Installer: Net-Installer directly from Debian Portal

``https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.4.0-amd64-netinst.iso``

Before using my link: **Please check Version first.**

## Installer

Mostly the following configurations during Installprocess:

*   Standard-User with ``sudo``

    Remark: simply leave Password for root Account empty!

*   Using **LVM** - at least for home, sometimes tmp, var

*   Install Apt-Alternative **Nala** from Debian 12 Repos

    ``sudo apt install nala``

*   If Desktop-Manager install **KDE Full**

*   ``sudo nala install kde-full``

*   Use this Repo with

    ``mkdir ~/gitlab-projekte && cd ~/gitlab-projekte``

    ``git clone https://gitlab.com/joebrandes/debian-i3-onedark.git``

    use Folder 99-DEB12BOOKWORM with Scripts of your choice

    ``sh XY.sh``
