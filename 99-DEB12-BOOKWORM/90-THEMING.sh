#!/bin/bash
# Again: i hope you know what you are doing ;-)

# OneDark
# Nord aka Nordic
# Catppucin
# Dracula
# Gruvbox

# GTK FOLDER: Icons / Themes
temp="$HOME/tmp"
icons="$HOME/.icons"
themes="$HOME/.themes"
[ ! -d "$icons" ] && mkdir -p "$icons"
[ ! -d "$themes" ] && mkdir -p "$themes"
[ ! -d "$temp" ] && mkdir -p "$temp"
echo 'Folders for Icons and Themes OK'

# Theme OneDark aka OneDarkPro
# ============================

cd ~/.themes && git clone https://github.com/UnnatShaneshwar/AtomOneDarkTheme
echo 'OneDark Theme cloned'

cd ~/.icons && git clone https://github.com/adhec/one-dark-icons.git
echo 'One Dark Icons cloned'

# Theme Nord aka Nordic
# =====================

cd ~/.themes && git clone https://github.com/EliverLara/Nordic
echo 'Nordic Theme cloned'

cd ~/tmp && git clone https://github.com/robertovernina/NordArc nordarc
cd nordarc
cp -a NordArc-Icons ~/.icons/
cp -a NordArc-Theme ~/.themes/
echo 'Nordarc Theme and Icons cloned'

# Theme Catppuccin
# ================

cd ~/tmp && wget https://github.com/catppuccin/gtk/releases/download/v0.5.0/Catppuccin-Frappe-Standard-Blue-Dark.zip
unzip Catppuccin-Frappe-Standard-Blue-Dark.zip
cp -a Catppuccin-Frappe-Standard-Blue-Dark ~/.themes

cd ~/tmp && git clone https://github.com/catppuccin/papirus-folders.git
cd papirus-folders
sudo cp -r src/* /usr/share/icons/Papirus

echo 'Catppuccin Theme cloned - for Icons look on github.com/catppuccin/papirus-folders'


# Theme Dracula
# =============

cd ~/.themes && git clone https://github.com/dracula/gtk.git dracula
echo 'Dracula Theme cloned'

cd ~/.icons && git clone https://github.com/m4thewz/dracula-icons.git
echo 'Dracula Icons cloned'

# Theme Gruvbox
# =============

cd ~/tmp && git clone https://github.com/TheGreatMcPain/gruvbox-material-gtk.git gruvbox
cp -a ~/tmp/gruvbox/themes/Gruvbox-Material-Dark ~/.themes/
echo 'Gruvbox Theme cloned'
cp -a ~/tmp/gruvbox/icons/Gruvbox-Material-Dark ~/.icons/
echo 'Gruvbox Icons cloned'

