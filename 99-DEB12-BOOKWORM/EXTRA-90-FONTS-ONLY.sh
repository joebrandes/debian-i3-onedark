# Fonts - no folder .fonts in naked system
fonts="$HOME/.fonts"
localsharefonts="$HOME/.local/share/fonts"
[ ! -d "$fonts" ] && mkdir -p "$fonts"
[ ! -d "$localsharefonts" ] && mkdir -p "$localsharefonts"

cp -R ../90-FONTS/.fonts/* ~/.fonts
cp -R ../90-FONTS/.local/share/fonts/* ~/.local/share/fonts/
echo '****************************************************'
echo 'MyFonts for ~/.fonts and ~/.local/share/fonts copied'
