# Tmux
mkdir -p ~/.tmux/plugins
git clone https://github.com/wfxr/tmux-power.git ~/.tmux/plugins/tmux-power
# Change with my tmux-power.tmux conf
cp ~/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux-original
cp ../30-tmux/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux
# do not forget your ~/.tmux.conf
cp ../30-tmux/.tmux.conf ~/.tmux.conf
