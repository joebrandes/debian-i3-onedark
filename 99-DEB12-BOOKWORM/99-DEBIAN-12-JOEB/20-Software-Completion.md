Table of contents

- [Software Completion - mainly for the terminal](#software-completion---mainly-for-the-terminal)
- [Kitty - Terminal of Choice](#kitty---terminal-of-choice)
- [Neovim - Terminal editor of Choice](#neovim---terminal-editor-of-choice)
  - [AppImage ("universal" Linux package)](#appimage-universal-linux-package)
  - [Linux Executables](#linux-executables)
  - [Neovim configuration: kickstart.nvim](#neovim-configuration-kickstartnvim)
- [Keyboard Dilemma](#keyboard-dilemma)
- [LSD - an alternitive to ls](#lsd---an-alternitive-to-ls)
- [Nemo as i3 DE Alternative to Dolphin on KDE](#nemo-as-i3-de-alternative-to-dolphin-on-kde)
- [Rofi - make Startmenu via Shortcuts](#rofi---make-startmenu-via-shortcuts)
- [Ranger - filemanager for the terminal](#ranger---filemanager-for-the-terminal)


I need a bit more stuff to feel fine on my Systems. 

    Terminal Tools like **Rofi** do not work with **Wayland** Composing!
    You need the **old/proper X-Server**!

I will show the **Wayland and Sway** tech-combo later.

# Software Completion - mainly for the terminal

I start with my ideas of my **i3** Window Manager environment and will
break down the supplimentary Software i need for my console based work.

I scrape everything **i3** related or extra **X-server** because of my
already full functioning KDE with **Wayland** and/or **X**.

```
Most stuff will go to ~/.local/bin which is automatically in 
path for Bash and or Zsh.
```

And i will leave out everything already installed like `firefox-esr`
or `evolution` or would be neccessary for i3-Panels like `gsimplecal`.

```
#!/bin/bash
# Update your system
sudo apt update && sudo apt upgrade -y

# Prepare local folders
mkdir ~/bin
mkdir -p ~/.local/bin
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/.local/share/applications
mkdir ~/.icons
mkdir ~/tmp
mkdir ~/opt

# Software installs
sudo apt install gedit vlc neofetch command-not-found net-tools wakeonlan lsd \
    zsh rxvt-unicode rofi ranger mc vim git make tmux curl wget cifs-utils \
    python3-full python3-pip python3-dev python3-pygit2 python3-venv python3-psutil \
    fzf fd-find ripgrep bat atool python3-pygments python3-pil \
    nemo fonts-noto fonts-font-awesome papirus-icon-theme fontforge libharfbuzz-bin \
    xsel xclip highlight htop figlet viewnior ueberzug \
    zathura zathura-pdf-poppler zathura-ps

curl -sS https://starship.rs/install.sh | sh

echo ' '
echo '*************'
echo 'Please REBOOT and you could copy the config stuff one-to-one'
echo '*************'

```
A few programs i will deploy differently because of the newer version
delivered by those install procedures.

*   kitty
*   neovim
*   Nautilus (Dateien) Alternative: Nemo

# Kitty - Terminal of Choice

The Kitty Developer makes the *Installation* of **Kitty** quiet easy. 
Therefor the Debian Packages Mantainers a pretty much not interested.

Right now (March 2024) the newest version ist 0.33 from 2024-03-12 and the
packaged Version is 0.26.5-5 from 2022-11-07 !

Just follow the Developers path to installation:

*   [Kitty website Developer Kovid Goyal](https://sw.kovidgoyal.net/kitty/)
*   [Kitty on Github](https://github.com/kovidgoyal/kitty)


You can install pre-built binaries of kitty if you are on macOS or Linux using the following simple command:

`curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin`

The binaries will be installed in the standard location for your OS, /Applications/kitty.app on macOS
and `~/.local/kitty.app` on Linux.

To be clear:

```
❯ ls -Al $(which kitty)
lrwxrwxrwx 1 seminar seminar 40 23. Feb 20:53 /home/seminar/.local/bin/kitty -> /home/seminar/.local/kitty.app/bin/kitty
```

Integration of **kitty.app** in DE: https://sw.kovidgoyal.net/kitty/binary/

```
# Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in
# your system-wide PATH)
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
# Place the kitty.desktop file somewhere it can be found by the OS
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
# If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
# Update the paths to the kitty and its icon in the kitty.desktop file(s)
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
```



The installer only touches files in that directory. To update kitty, simply re-run the command.

For **kitty** theming use the prepared files in folder (you may alread use them / copied them):

*   `40-kitty/.config/kitty/*` - Kittey Themes / Standard-Theme-Name: `kitty.conf`
*   `40-kitty/.local/share/nemo/actions/open_in_kitty.nemo_action` - speaks for itself

**Gnome Terminal**

If you like to stay and use the Standardterminal **Gnome Terminal** feel free
to change to **Theme Nord** via https://github.com/nordtheme/gnome-terminal.

You should then install `sudo apt install uuid-runtime dconf-editor` to perform the tasks
for theming Gnome Terminal.

# Neovim - Terminal editor of Choice

I offer information to two separat Deployments of **Neovim**

## AppImage ("universal" Linux package)

The following is a copy of 
[Github Repo Install infos](https://github.com/neovim/neovim/blob/master/INSTALL.md).

The Releases page provides an AppImage that runs on most Linux systems. 
No installation is needed, just download nvim.appimage and run it. 
(It might not work if your Linux distribution is more than 4 years old.)

```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
./nvim.appimage
```

To expose nvim globally:

```
mkdir -p /opt/nvim
mv nvim.appimage /opt/nvim/nvim
```

And the following line to ~/.bashrc:

```
export PATH="$PATH:/opt/nvim/" 
```

If the ./nvim.appimage command fails, try:

```
./nvim.appimage --appimage-extract
./squashfs-root/AppRun --version

# Optional: exposing nvim globally.
sudo mv squashfs-root /
sudo ln -s /squashfs-root/AppRun /usr/bin/nvim
nvim
```

## Linux Executables

Neovim in Debian 12 only in version **0.7.2-7** while i am *recording* this.
The newest version is **0.9.5**!

*   [Neovim Website](https://neovim.io/)
*   [Neovim Github](https://github.com/neovim/neovim)

We will use the Pre-built archives for a system-wide **Neovim**:

The Releases page provides pre-built binaries for Linux systems.

```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz
```

After this step add this to `~/.bashrc`: (already in my `.bash_aliases`!)

`export PATH="$PATH:/opt/nvim-linux64/bin"`

Without any further Configuration run `:checkhealth`

I would go no further ;-) - but if you want to...

You should think twice about the needed Neovim/Python3 Environment install:

`sudo apt install python3-neovim` is a problem because it install **old Neovim** from Debian Repos!

But that at least solves Neovim *problems* with Clipboard package `xclip` and other stuff.

One of possible Starts with Neovim ist LazyVim (as Plugin Manager)
or complete Environment like:

`git clone https://gitlab.com/joebrandes/lazyvim-config ~/.config/nvim`

Latest test in February 2024 checked out - system functioning but lots and lots of
messages while using because of all the missing **LSP** and **Scripting Env** stuff.

## Neovim configuration: kickstart.nvim

* [YouTube Vid Kickstart.nvim TJ de Vries](https://www.youtube.com/watch?v=m8C0Cq9Uv9o&t=163s)
* [Github Repo kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)

Especially be reminded of tech to separate diferent Neovim Configs:

`git clone https://github.com/nvim-lua/kickstart.nvim.git ~/.config/nvim-kickstart`

With an Alias in your Shell config like:

`alias nvim-kickstart='NVIM_APPNAME="nvim-kickstart" nvim'`

That gives you `nvim-kickstart` to use `nvim` with a special conf-Env.

```
_ ZSH ❯ ls -Al ~/.config/nvim* -d
drwxr-xr-x 4 joeb joeb 4096 17. Mär 17:17 /home/joeb/.config/nvim
drwxr-xr-x 6 joeb joeb 4096 24. Mär 14:28 /home/joeb/.config/nvim-kickstart

_ ZSH ❯ ls -Al ~/.local/share/nvim* -d
drwxr-xr-x 6 joeb joeb 4096 18. Mär 17:19 /home/joeb/.local/share/nvim
drwxr-xr-x 4 joeb joeb 4096 24. Mär 14:28 /home/joeb/.local/share/nvim-kickstart
```

You can do this with any other form of Neovim Configurations/Distros!

# Keyboard Dilemma

Most of the stuff on the net - espacially for Neovim ist using Shortcuts
optimized for **US Keyboard Layouts**. Even standard Coding is pretty much
based on the fact that Braces are mostly one-klick-available - not so much
on European/German Layouts!

* [Arch Linux Wiki - Keyboard Configurations](https://wiki.archlinux.org/title/Xorg/Keyboard_configuration)
* [How to check the Layout](https://www.baeldung.com/linux/current-keyboard-layout)

Just a few hints and tipps: Tools `setxkbmap`, `xkeycaps`, `xkbprint`

* `setxkbmap -layout us` - use US Layout
* `xkeycaps` - switch Layout (and more) via X/GUI
* `setxkbmap -option -print -verbose 10` - full infos for Layout
* `xkbprint -color ":0" - | ps2pdf - > current_keyboard_layout.pdf` - create Layout PDF

![xkbprint of US Layout](./PICS/current_keyboard_layout_US.jpg)

# LSD - an alternitive to ls

Simple Installation of the newest version including the support for
color management via `.config/lsd/colors.yaml` combined with `.config/lsd/config.yaml`.

```
The standard **lsd** tool of Debian is old and should be updated
to work with my Configuration stuff!
```

Download DEB (without any dependencies!): https://github.com/lsd-rs/lsd

And then: `dpkg -i lsd_1.0.0_arm64.deb` (or whatever newer version)

For Coloring: https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg

# Nemo as i3 DE Alternative to Dolphin on KDE

Nemo really Works nicely with `papirus-icon-theme` and can use folder-colors 
and has a bit more Tech onBoard.

[Papirus Folder Color changer](https://github.com/PapirusDevelopmentTeam/papirus-folders)

Then change standard to **nordic**: `papirus-folder -C nordic`

You can get even more Nord-Colors with https://github.com/Adapta-Projects/Papirus-Nord.

The Problem is: because i chose KDE Desktop Environment there is a lot of
functions in Nemo that do not work out of the box.

An Impression to start of with: (Theme: Nord)

![Screenshot Debian 12 with Nemo and Actions for XTerm and Kitty](./PICS/nemo-actions.png)


We can help us with Nemo **Actions**. The upcoming Examples are to files 
in `~/.local/share/nemo/actions` called

*   open_in_kitty.nemo_action
*   open_in_xterm.nemo_action
   
with the following Code:

```
[Nemo Action]

Name=Open in Kitty
Comment=Open the 'kitty' terminal in the selected folder
Exec=/home/joeb/.local/bin/kitty --working-directory="%F"
Icon-Name=kitty
Selection=any
Extensions=dir;
```

and

```
[Nemo Action]

Name=Open in XTerm
Comment=Open the 'XTerm' terminal in the selected folder
Exec=xterm -e 'cd %F && /bin/zsh'
Icon-Name=xterm
Selection=any
Extensions=dir;
```

Especially the xterm solution is very *top of the notch*!
I found it on an old 2008 [post on linuxquestions.org](https://www.linuxquestions.org/questions/linux-software-2/xterm-howto-lauch-an-xterm-into-a-specific-directory-226013/).

# Rofi - make Startmenu via Shortcuts

There are all neccessary Dotfiles available in the underlying Chapters
for these tools.

Folders:

*   `70-rofi/.config/rofi` - config file for Rofi
*   `70-rofi/.local/share/rofi/themes` - theming for Rofi - or `themes` as subfolder in `.config/rofi`

Shorcut for Rofi: `rofi -show drun -theme nord` with keys `Super` + `d` for me.

Or one of the longer versions provieded as examples in the **i3 configs**.

# Ranger - filemanager for the terminal

Again: all the configuration is available in main project files.

Folder: `7-ranger/.config/ranger/..`

Example: For image previews in **ranger** set configuration in `rc.conf` to `set preview_images_method ueberzug` (or kitty, if problems occur).


