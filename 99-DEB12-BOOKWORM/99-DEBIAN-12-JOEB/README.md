Table of Contents

- [Intro to Install Debian 12 with **i3 Window Manager** and KDE (from Tasksel)](#intro-to-install-debian-12-with-i3-window-manager-and-kde-from-tasksel)
  - [Basic Install](#basic-install)
  - [Workspaces](#workspaces)
- [Installation Chapters](#installation-chapters)


An Impression to start of with: (Theme: Nord)

![Screenshot Debian 12 with PopOS Tiling WM and Nemo, Ranger, Kitty and Rofi](./PICS/debian12-i3-nord.png)

And of we go...


# Intro to Install Debian 12 with **i3 Window Manager** and KDE (from Tasksel)

Use cases: For fresh Installs and My Machines

```
There should be definitely a Hyper-V, PVE and/or KVM/qemu VM built with this
```

A few short informations up front:

## Basic Install

+   User: **joeb** / Pass: **\*********\** (with sudo)
+   Graphical Install with e.g.
    60 GB Virtual HDD (LVM Support without Partitions)ra
    or 127 GB Standard Size for Hyper-V
    and DE KDE from Tasksel with SSH active
+   I use classic **i3 AND X.org - see: Rofi and Co**
+   Themming: Nord (as an Example - see www.nordtheme.com)
+   XTerm as main Terminal - Alternatives: Kitty, URxv

## Workspaces

We want put the keyboard standards `Super` + `#` to good use:

1.  **Browser** - Tool: `firefox-esr` - Firefox (Browser)
2.  **Files** - Tool: `nemoe` - Dateien / Files (or Thunar, ...)
3.  **Terminal** - Tool: `xterm` - Terminal (or Kitty, URxvt, ...)
4.  **Coding** - Tool: `vscode` - Texteditor (Coding - or `neovim`, ...)
5.  **VM** - Tool: `virt-manager` for KVM/qemu
6.  **Office** - Tool: `onlyoffice`
7.  **Email** - Tool: `thunderbird` - Mail, Contacts, Calendar
8.  **Graphics** - Tool: `gimp` 
9.  **System** - Tool: (e.g.) `remmina`, ...

So as always - on my systems - use `Super` + `3` for your terminal.

And as always: `Super` is (mostly ;-) your `Win` key.

We want those standard applications to go to our standard **Workspaces** automatically!
So - obviously - we use a WM of our Choice. I'm perfectly happy with **i3**
and will definetely switch over to **Wayland with Sway** when it's time...

---

    REMINDER:

    Use Snapshot - or whatever your Tech or Virtualization Environment calls it - to freeze your
    Installation steps!

---

# Installation Chapters

More Chapters with further Installations and Improvements:

*   [NALA - Package Management improvement to apt Tools](10-NALA.md)
*   [Software Completion - more Tools to have the Feel of Tiling Window Manager](20-Software-Completion.md)
