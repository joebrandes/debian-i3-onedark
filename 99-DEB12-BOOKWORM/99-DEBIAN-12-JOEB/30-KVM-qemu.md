TOC:

- [KVM / qemu](#kvm--qemu)
  - [Links](#links)
- [In case of BSOD this may help...](#in-case-of-bsod-this-may-help)
- [Installations](#installations)
- [Check Environment](#check-environment)
- [Virtio ISO for Windows](#virtio-iso-for-windows)
- [Check Nested Virtualization](#check-nested-virtualization)
- [Export / Import of KVM VMs](#export--import-of-kvm-vms)



# KVM / qemu

Obviously we need Virtualization! And i provide myself with ProxmoxVE
in my Home(lab) Environment.

But i want VM directly in my daily driver: so KVM/qemu it is.

## Links

*   [Complete Sysguides-Guide to KVM/qemu: How Do I Properly Install KVM on Linux](https://sysguides.com/install-kvm-on-linux)
*    [Sysguides: How to Properly Install a Windows 11 Virtual Machine on KVM](https://sysguides.com/install-a-windows-11-virtual-machine-on-kvm)   
*    [Virtio ISO Download](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)
*    [Nested Virtualization](https://discourse.ubuntu.com/t/how-to-enable-nested-virtualization/41694)
*    [Nested Virtualization - Hyper-V 2019 in qemu-kvm](https://www.redpill-linpro.com/techblog/2021/04/07/nested-virtualization-hyper-v-in-qemu-kvm.html)
*    [Forum entry to BSOD for Windows Systems](https://forum.level1techs.com/t/windows-10-1803-as-guest-with-qemu-kvm-bsod-under-install/127425/10)
*    [To BSOD see also](https://discussion.fedoraproject.org/t/fedora-36-windows-11-guest-in-qemu-kvm-keeps-crashing-on-startup/64292/2)


# In case of BSOD this may help...

Some text from https://forum.level1techs.com/t/windows-10-1803-as-guest-with-qemu-kvm-bsod-under-install/127425/13 :

*In case other people have the same issue I had to setup the cpu to be a 
core2duo during the windows 10 installation process. After the windows 
installation, switching back to host-passthrough with the options kvm ignore_msrs=1 on /etc/modprobe.d/kvm.conf 
worked fine.*

So create `etc/modprobe.d/kvm.conf`with code `options kvm ignore_msrs=1` and reboot.

# Installations

Remark: see **How Do I Properly Install KVM on Linux** (Links above)

```
# Basic stuff
sudo apt install qemu-system-x86 libvirt-daemon-system virtinst \
    virt-manager virt-viewer ovmf swtpm qemu-utils \e
    guestfs-tools libosinfo-bin tuned
```

A brief description of the packages listed above.

*   qemu-kvm/qemu-system-x86/qemu-full: A user-level KVM emulator that facilitates communication between hosts and VMs.
*   libvirt/libvirt-daemon-system: A daemon that manages virtual machines and the hypervisor as well as handles library calls.
*   virt-install/virtinst: A command-line tool for creating guest virtual machines.
*   virt-manager: A graphical tool for creating and managing guest virtual machines.
*   virt-viewer: A graphical console for connecting to a running virtual machine.
*   edk2-ovmf/ovmf: Enables UEFI support for Virtual Machines.
*   swtpm: A TPM emulator for Virtual Machines.
*   qemu-img/qemu-utils: Provides tools to create, convert, modify, and snapshot offline disk images.
   
Extras:

*   guestfs-tools: Provides a set of extended command-line tools for managing virtual machines.
*   libosinfo/libosinfo-bin: A library for managing OS information for virtualization.
*   tuned: A system tuning service for Linux.

# Check Environment

```
lsmod | grep -i kvm                            # are kvm modules loaded?
cat /sys/module/kvm_intel/parameters/nested    # is nested Virt possible?
sudo systemctl enable libvirtd.service         # daemons running?
sudo virt-host-validate qemu                   # everything supported?
sudo nmcli device status                       # Bridge available - better: look for Network bridge
```

To enable VT-d, open the `/etc/default/grub` file and 
add 'intel_iommu=on iommu=pt' to the GRUB_CMDLINE_LINUX line.

```
#### For Intel CPU
$ sudo vim /etc/default/grub
...
GRUB_CMDLINE_LINUX="... intel_iommu=on iommu=pt"
...
```

If you have an AMD CPU, IOMMU is enabled by default. To enable pass-through mode, just add iommu=pt.

The obvious: do a `sudo update-grub`, reboot an check again.


# Virtio ISO for Windows

```
wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso
```

# Check Nested Virtualization

You can use same VM to boot a Ubuntu live cd, 
virtualization should be already enabled (in the guest):

```
(guest)$ lscpu | grep -i Virtualiz
Virtualization:                  VT-x

(guest)$ lscpu | grep vmx | grep lm
Flags:                           fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon rep_good nopl cpuid pni pclmulqdq vmx ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves arat

(guest)$ kvm-ok
INFO: /dev/kvm exists
KVM acceleration can be used
```

# Export / Import of KVM VMs

*   [Example article on ostechnix.com](https://ostechnix.com/export-import-kvm-virtual-machines-linux/) 


In Short and Examples: 


Export: 
 
```
virsh dumpxml vm-name > /path/to/xm_file.xml 
virsh dumpxml ubuntu20.04-clone > ~/Documents/ubuntu.xml  
# cp is not good without sparse and not for cp via network
sudo cp /var/lib/libvirt/images/ubuntu20.04-clone.qcow2 ~/Documents/  
# rsync - better with sparse and networking - please install on both sides!
rsync -aS ~/Downloads/kvm-win11-sysguide.qcow2 user@192.168.2.172:/var/lib/libvirt/images
```
 
Import: 

```
virsh define --file <path-to-xml-file> 
virsh define –file ~/Documents/ubuntu.xml  
# cp with sparse
sudo cp cp --sparse=always ~/Documents/ubuntu20.04-clone.qcow2 /var/lib/libvirt/images   
```