# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/joeb/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#

# ANTIGEN Solution for Plugins:
# =============================================================================
# https://github.com/zsh-users/antigen
# https://github.com/zsh-users/antigen/wiki/Quick-start
# curl -L git.io/antigen > antigen.zsh
# or use git.io/antigen-nightly for the latest version
source $HOME/antigen.zsh
# antigen commands: antigen help
# =============================================================================
#
antigen use oh-my-zsh
# plugins in:  ~/.antigen/bundles/robbyrussel/oh-my-zsh/...
antigen bundle git
antigen bundle colorize
antigen bundle colored-man-pages
antigen bundle alias-finder
# plugins in: ~/.antigen/bundles/zsh-users/...
antigen bundle zsh-users/zsh-completions
antigen bundle z
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

# apply bundles
antigen apply
# =============================================================================



# FZF - RIPGREP
# =============================================================================
# https://github.com/junegunn/fzf
# fzf environment and exports:
# Original Debian:
# . /usr/share/doc/fzf/examples/key-bindings.zsh
# . /usr/share/doc/fzf/examples/completion.zsh
. $HOME/.config/fzf/examples/key-bindings.zsh
. $HOME/.config/fzf/examples/completion.zsh
export FZF_DEFAULT_OPTS='--height 40% --border'
# use ripgrep with FZF instead of find
# export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'
# more commands:
export FZF_ALT_C_COMMAND='find .'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# configuration for ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# =============================================================================


# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# ZSH-Plugin COLORIZE - see Chapter with antigen above
# =============================================================================
# provides tools colorize_cat (alias ccat)
# and colorize_less (cless):
#
if [ -n "$(command -v colorize_cat)" ] && [ -n "$(command -v colorize_less)" ]; then
    alias cat='colorize_cat'
    alias less='colorize_less'
fi
# =============================================================================


# EXPORTS
# put my Exports here - later in extra file?
# =============================================================================
#
# ZSH Colorize Style
export ZSH_COLORIZE_STYLE="native"

# fixing Exports for RANGER standard Editor vim or nvim
export VISUAL=vim
export EDITOR=vim

# make virsh *remember* the right qemu context
export LIBVIRT_DEFAULT_URI='qemu:///system'

# get nala going:
apt() {
  command nala "$@"
}
sudo() {
  if [ "$1" = "apt" ]; then
    shift
    command sudo nala "$@"
  else
    command sudo "$@"
  fi
}

# Alias for nvim-kickstart
alias nvim-kickstart='NVIM_APPNAME="nvim-kickstart" nvim'

# python venvs
source ~/.local/share/python3/bin/activate

# STARSHIP.RS
# =============================================================================
#
eval "$(starship init zsh)"
