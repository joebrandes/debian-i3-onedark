# a Collection of stuff for the Bash Shell Console
# just Snippets!!

# ###########################################################################
# START - Stuff from a working LinMint System - back in the Days
# ###########################################################################

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color | *-256color) color_prompt=yes ;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

if [ "$color_prompt" = yes ]; then
	PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
#    ;;
#*)
#    ;;
#esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

###########################################################
# MY-STUFF - please consider the software in need:
# for powerline-go: go and git (see software baseinstalls)
###########################################################

if [ -e $HOME/.bashrc_JB_MY-STUFF ]; then
	source $HOME/.bashrc_JB_MY-STUFF
	echo 'Extras for .bashrc loadad with .bashrc_JB_MY-STUFF'
fi

# Scriptprobs with bash
# . ~/.config/tools/colorize.plugin.zsh

# ###########################################################################
# END - Stuff from a working LinMint System - back in the Days
# ###########################################################################

# ###########################################################################
# Start - Extra Source Stuff from a working LinMint System - back in the Days
# ###########################################################################

# .bashrc_JB_MY-STUFF - extra stuff expanding the standard .bashrc
# ================================================================

# my own pager with most: sudo apt install most
# or manual color stuff for less and man-pages
if [ -x "$(command -v most)" ]; then
	export PAGER=most
else
	# Tipp mit Farbe für less:
	# https://www.linux-community.de/blog/bunte-manpages-anzeigen-mit-most-und-less/
	export LESS_TERMCAP_mb=$(printf "\e[1;31m")    # Blinken
	export LESS_TERMCAP_md=$(printf "\e[1;31m")    # Fett
	export LESS_TERMCAP_me=$(printf "\e[0m")       # Modus Ende
	export LESS_TERMCAP_se=$(printf "\e[0m")       # Ende Standout-Modus
	export LESS_TERMCAP_so=$(printf "\e[1;44;33m") # Start Standout-Modus (Info-Kasten)
	export LESS_TERMCAP_ue=$(printf "\e[0m")       # Unterstreichen Ende
	export LESS_TERMCAP_us=$(printf "\e[1;32m")    # Unterstreichen Anfang
fi

# my own aliases - joebstyle
alias jbdf='df -h | grep ^/dev | grep -v ^/dev/loop'

# sudo apt install fortunes cowsay tty-clock
if [ -x "$(command -v cowsay)" ] && [ -x "$(command -v fortune)" ]; then
	# Bit of Fun with cowsay and fortune cookies
	alias jbcowsay='while true; do fortune|cowsay -f tux && sleep 20; done'
fi

if [ -x "$(command -v tty-clock)" ]; then
	alias jbtty-clock="tty-clock -s -f '%A, %d.%m.%Y'"
fi

# more fun with bat/batcat instead of cat
# sudo apt install bat # ubuntu 20.04: Befehl dann batcat - Manpage auch  mit batcat
if [ -x "$(command -v batcat)" ]; then
	alias jbcat="PAGER=less;batcat"
fi

#  individuelle Wallpaper auf 4 Arbeitsflächen!
if [ -x "$(command -v $HOME/Bilder/wallpaper/wallpaper.sh)" ]; then
	alias jbwallpaper="$HOME/Bilder/wallpaper/wallpaper.sh"
fi

# Projektordner mit  VSCODIUM aka codium starten
if [ -x "$(command -v codium)" ]; then
	alias jbcode="codium . &"
fi

# Passwords mystyle with Tool makepasswd: 15 Characters out of string
if [ -x "$(command -v makepasswd)" ]; then
	alias jbmakepasswd="makepasswd --chars 15 --string 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ#+-()=}][{;:'"
fi

# imagemagick mogrify tool for web-pics 800px
if [ -x "$(command -v mogrify)" ]; then
	alias jbmogrify="mogrify -path _web-800px/ -resize 800 -quality 90 -format jpg *.png"
fi

# ===========================
# Powerline-Go mit golang-go
# ===========================
# in case of problems with https and/or Security stuff:
# git config --global http.sslVerify false
# and:
# go get -u -insecure github.com/justjanne/powerline-go
# Default stuff:
# ==============
# Remember: install the powerline-fonts aka fonts-powerline and the go-stuff
# https://awesomeopensource.com/project/justjanne/powerline-go
# https://github.com/justjanne/powerline-go
# folder ~/go creating with (needs go and git (needs go and git):
# go get -u github.com/justjanne/powerline-go

export GOPATH="$HOME/go"

function _update_ps1() {
	PS1="$($GOPATH/bin/powerline-go -error $? -newline -theme default -shell bash -modules-right time)"
}

if [ "$TERM" != "linux" ] && [ -f "$GOPATH/bin/powerline-go" ]; then
	PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

# Finish up with the terminals
# Colors for the console with solarized dark
# eval $(dircolors -b ~/.bash_dircolors_solarized_dark)
if [ $TERM == "xterm-256color" ] && [ -e ~/.bash_dircolors_solarized_dark ]; then
	eval $(dircolors -b ~/.bash_dircolors_solarized_dark)
else
	[ $TERM = "linux" ] && [ -e ~/.bash_dircolors_solarized_ansi ]
	eval $(dircolors -b ~/.bash_dircolors_solarized_ansi)
fi

# nodejs stuff
# ============
if [ -x $HOME/opt/nodejs/bin/node ]; then
	export PATH=$HOME/opt/nodejs/bin:$PATH
fi

# use npm gulp tar version of nodejs
if [ -f "/home/joeb/opt/nodejs/bin/gulp" ]; then
	alias gulp='/home/joeb/opt/nodejs/bin/gulp'
fi

# Home-Dir bin - if not already set by Distro
# ============
echo $PATH | egrep "$HOME/bin" >>/dev/null
if [ $? -ne 0 ] && [ -d "$HOME/bin" ]; then
	export PATH=$HOME/bin:$PATH
fi

# ###########################################################################
# End - Extra Source Stuff from a working LinMint System - back in the Days
# ###########################################################################
