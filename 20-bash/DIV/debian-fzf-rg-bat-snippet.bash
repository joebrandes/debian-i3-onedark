# FZF - RIPGREP - bat/batcat (on Debian Distro)
# =============================================================================
# https://github.com/junegunn/fzf
# fzf environment:
. /usr/share/doc/fzf/examples/key-bindings.bash
. /usr/share/doc/fzf/examples/completion.bash
#
# fzf exports:
export FZF_DEFAULT_OPTS='--height 40% --border --preview="batcat --style numbers,changes --color=always {}"'
# use ripgrep with FZF instead of find
# export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'
# more commands:
export FZF_ALT_C_COMMAND='find .'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# configuration for ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# =============================================================================
# CDD - Change Directory with Dialogue
alias cddhome="cd ~ && cd \$(find . -type d | fzf)"
alias cddprojects="cd /var/www/html && cd \$(find * -type d | fzf)"
