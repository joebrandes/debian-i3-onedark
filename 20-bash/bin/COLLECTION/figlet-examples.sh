#!/bin/sh

# Prints "JoeB" using every font installed on /usr/share/figlet
# Written by Sandro Tosi <matrixhasu@gmail.com>
# Modified by Jonathan McCrohan <jmccrohan@gmail.com>

# or look: https://patorjk.com/software/taag/#p=display&f=3D-ASCII&t=I3%20Joe%20B.

for file in /usr/share/figlet/*.flf ; do
   FONT=`basename $file | sed 's|\(.*\)\.\(.*\)|\1|'`

   printf "Font: $FONT\n"
   figlet -f $FONT JoeB
   printf "\n"
done
