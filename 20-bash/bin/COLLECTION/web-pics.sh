#!/bin/bash
# Scripting for Web Pics 800px width from Screenshots *.png

# Variables:
count=1
DIR=$HOME/Bilder/web-pics/_web-800px

# Workdir existing?
[ ! -e $DIR ] && exit 1

# imagemagick mogrify tool for web-pics 800px
if [ -x "$(command -v mogrify)" ]; then
    mogrify -path $DIR/ -resize 800 -quality 90 -format jpg *.png
fi

# images - now as jpg - rename in screenshot.$$.jpg
for datei in $(find $DIR -name "*.jpg" -print 2>/dev/null)
    do
        mv $datei $DIR/screenshot.0${count}.jpg
        count=$(expr $count + 1)
done

echo "$(expr $count - 1) images in 800px and renamed"
ls -alh $DIR


