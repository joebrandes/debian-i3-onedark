#!/bin/bash
# see: https://github.com/dylanaraps/neofetch/blob/master/neofetch
# ----------------------------------------------------------------
#
#

# function from neofetch
# Color Blocks
# Color block range
# The range of colors to print.
#
# Default:  '0', '15'
# Values:   'num'
# Flag:     --block_range
#
# Example:
#
# Display colors 0-7 in the blocks.  (8 colors)
# neofetch --block_range 0 7
#
# Display colors 0-15 in the blocks. (16 colors)
# neofetch --block_range 0 15
block_range=(0 15)
# Toggle color blocks
#
# Default:  'on'
# Values:   'on', 'off'
# Flag:     --color_blocks
color_blocks="on"
# Color block width in spaces
#
# Default:  '3'
# Values:   'num'
# Flag:     --block_width
block_width=6
# Color block height in lines
#
# Default:  '1'
# Values:   'num'
# Flag:     --block_height
block_height=2
# Color Alignment
#
# Default: 'auto'
# Values: 'auto', 'num'
# Flag: --col_offset
#
# Number specifies how far from the left side of the terminal (in spaces) to
# begin printing the columns, in case you want to e.g. center them under your
# text.
# Example:
# col_offset="auto" - Default behavior of neofetch
# col_offset=7      - Leave 7 spaces then print the colors
col_offset="auto"

get_cols() {
	local blocks blocks2 cols

	if [[ "$color_blocks" == "on" ]]; then
		# Convert the width to space chars.
		printf -v block_width "%${block_width}s"

		# Generate the string.
		for ((block_range[0]; block_range[0] <= block_range[1]; block_range[0]++)); do
			case ${block_range[0]} in
			[0-7])
				printf -v blocks '%b\e[3%bm\e[4%bm%b' \
					"$blocks" "${block_range[0]}" "${block_range[0]}" "$block_width"
				;;

			*)
				printf -v blocks2 '%b\e[38;5;%bm\e[48;5;%bm%b' \
					"$blocks2" "${block_range[0]}" "${block_range[0]}" "$block_width"
				;;
			esac
		done

		# Convert height into spaces.
		printf -v block_spaces "%${block_height}s"

		# Convert the spaces into rows of blocks.
		[[ "$blocks" ]] && cols+="${block_spaces// /${blocks}[mnl}"
		[[ "$blocks2" ]] && cols+="${block_spaces// /${blocks2}[mnl}"

		# Add newlines to the string.
		cols=${cols%%nl}
		cols=${cols//nl/
[${text_padding}C${zws}}

		# Add block height to info height.
		((info_height += block_range[1] > 7 ? block_height + 2 : block_height + 1))

		case $col_offset in
		"auto") printf '\n\e[%bC%b\n' "$text_padding" "${zws}${cols}" ;;
		*) printf '\n\e[%bC%b\n' "$col_offset" "${zws}${cols}" ;;
		esac
	fi

	unset -v blocks blocks2 cols

	# Tell info() that we printed manually.
	prin=1
}

get_cols
