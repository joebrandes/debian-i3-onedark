#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# Existing .bashrc and .bash_aliases backup
mv ~/.bashrc ~/.bashrc-$(date -I)
[ -f $HOME/.bash_aliases ] && mv ~/.bash_aliases ~/.bash_aliases-$(date -I)

# Bash
cp .bashrc ~
cp .bash_aliases ~
echo '.bashrc and .bash_aliases copied'
