# BASH

The defacto standard for many Linux-based Distros.

We start out with the Standard ``.bashrc`` from ``/etc/skel``
in a fresh user homeprofile

Some Ideas:

*   Put all your personal stuff outside the vanilla ``.bashrc``
    and just ``source`` that stuff

    On Debian there is already a hint: ``~/.bash_aliases``.

*   Use **Starship.rs** - it just easy going and my prompts look
    similar in different Shells (even PowerShell with Windows)

*   Keep it simple and clean!

## Bash Deployment Idea

After an analysis of the regular Bash Environment at the
start of an userprofile (Debian user) i chose the
following approach.

*   Backup an existing ``~/.bashrc``
*   Provide a modified copy of the skeleton bashrc

    *   Add commentary and marker to make changes traceable
    *   Modifiy History parameter

*   Deploy own ``~/.bashrc`` and ``~/.bash_aliases``


## Analysis Debian (Status: Debian 12 Bookworm)

As always: we analyze what we are starting with!

The following Files will be used in the same order. The *user work*
should concentrate on the ``~/.bashrc`` which is last.

###🔧  ``/etc/profile`` (**System**)

Systemwide Profile file for **sh**, **bash**, **ksh**, **ash** (compatible)

<details><summary>📚 Details of file <code>/etc/profile</code></summary>

*   **PATH**

    Definition (and export) of Pathes

    <table>
        <tr>
            <th>User</th>
            <th>Paths</th>
        </tr>
        <tr>
            <td>ID0 / root</td>
            <td><code>/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin</code</td>
        </tr>
        <tr>
            <td>User</td>
            <td><code>/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games</code</td>
        </tr>
    </table>

*   **bash.bashrc**

    Loading ``/etc/bash.bashrc``

*   **PROMPT**

    Setting Prompts ``# `` or ``$ ``  for ``/bin/sh``

*   **profile.d with bash_completion.sh**

    Loads/Sources extra Profile scripts from ``/etc/profile.d/*.sh``

    Script for handling Bash Completion: ``bash_completion.sh``

    Sourcing ``/usr/share/bash-completion/bash_completion``

</details>

Which brings (sources) the next systemwide Config for Bash:

###🔧 ``/etc/bash.bashrc`` (**System**)

Systemwide Bash runtime **configuration**

<details><summary>📚 Details of file <code>/etc/bash.bashrc</code></summary>

*   **NO Prompt $PS1 -> do not use**

    If not running interactively, don't do anything

*   **chroot stuff**

    Define Variable for chroot environment

*   **PS1 prepare for non sudo-user**

    <code>PS1='...\u@\h:\w\$ '</code>

*   **command-not-found** using

    Package should be installed! Well known from my Suse times ;-)

</details>

This leaves us with a SystemConf to start with.

And we come now to the user realm.

###🔧  ``~/.profile`` (**Userprofile**)

Userprofile file for **bash** but only with there is **no**
special ``~/.bash_profile`` or ``~/.bash_login`` available.

<details><summary>📚 Details of file <code>~/.profile</code></summary>

*   **Source <code>~/.bashrc</code> for Bash**

    Using Bash with existing .bashrc

*   **ADD PATHES: <code>~/bin</code> and <code>~/.local/bin</code>**

    Check for Directories and add them to PATH

</details>

The file has a reference to **umask** setting in /etc/profile, which
is not done here! The **umask** config lies in ``/etc/login.defs``.

###🔧  ``~/.bashrc`` (**Userprofile**)

Runtime Configuration file for **bash** but only for
**non-login shells**.

📚 Details of file <code>~/.bashrc</code>

*   **History**

    Ignore duplicates and lines that start with space.

    ``HISTCONTROL=ignoreboth``

    Append - don't overwrite

    ``shopt -s histappend``

    Setting Sizes - **feel free to make them bigger**

    ``HISTSIZE=1000``

    ``HISTFILESIZE=2000``

*   **Color in Prompt**: Set var for color prompt - we want **yes**

        case "$TERM" in
            xterm-color|*-256color) color_prompt=yes;;
        esac

    Optimize for my various **xterm...**

        case "$TERM" in
            xterm* | xterm-color|*-256color) color_prompt=yes;;
        esac

    This goes into the Prompt $PS1

*   **ls with color**

    In the dircolors decide for ``ls='ls --color=auto``

*   **provide include idea for Aliases**

    Use ``~/.bash_aliases`` for external definitions

*   **bash-completion**

    Make sure the CLI works properly.
