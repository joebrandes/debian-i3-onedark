# NALA

Info Sources:

*   Gitlab Repo:
    https://gitlab.com/volian/nala
*   Wiki:
    https://gitlab.com/volian/nala/-/wikis/Installation
*   Chris Titus:
    https://christitus.com/stop-using-apt/

Status Start 2024:

*   Version 0.14 from October 2023
*   Debian 12 Bookworm Repo: Version 0.12.2 from February 2023

Conclusion: install **Nala** from Deb12 Repos!

``sudo apt install nala``

And follow up with optimizations: (new nala-sources.list Repo !!)

``sudo nala fetch``

Remark: installs a lot of Python3 stuff.

A few Infos from the Gitlab Repo:

## Parallel Downloads

Outside of pretty formatting, the number 1 reason to use Nala over apt is parallel downloads.

Nala will download 3 packages at a time per unique mirror in your sources.list file. This constraint is to limit how hard Nala hits mirrors. Opening multiple connections to the same mirror is great for speeding up downloading many small packages.

Additionally we alternate downloads between the available mirrors to improve download speeds even further. If a mirror fails for whatever reason, we just try the next until all defined mirrors are exhausted.

Note: Nala does not use APT for package downloading and verification

## Fetch

Which brings us to our next standout feature, nala fetch.

This command works similar to how most people use netselect and netselect-apt. nala fetch will check if your distro is either Debian or Ubuntu. Nala will then go get all the mirrors from the respective master list. Once done we test the latency and score each mirror. Nala will choose the fastest 3 mirrors (configurable) and write them to a file.

At the moment fetch will only work on Debian, Ubuntu and derivatives still tied to the main repos. Such as Pop!_OS

**IMPORTANT**: Nala creates new ``/etc/apt/sources.list.d/nala-sources.list``

##  History

Our last big feature is the nala history command.

If you're familiar with dnf this works much in the same way. Each Install, Remove or Upgrade we store in /var/lib/nala/history.json with a unique <ID> number.

At any time you can call nala history to print a summary of every transaction ever made. You can then further manipulate this with commands such as nala history undo <ID> or nala history redo <ID>.

If there is something in the history file that you don't want you can use the nala history clear <ID> It will remove that entry. Alternatively for the clear command we accept --all which will remove the entire history.

## Zsh/fish Completions

Nala's bash, Zsh and fish completions are now handled with typer.

There is nothing you need to do but install Nala and restart your shell for them to work
