# VIFM

Link: https://vifm.info/

For Debian: AppImage available

For Previews: https://github.com/thimc/vifmimg

Standard-config for Linux/Unix:

``~/.config/vifm/..``

Use AppData for Windows:

``$env:AppData\vifm\..``
