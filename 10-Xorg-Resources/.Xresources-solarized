! ------------------------------------------------------------------------------
! Colour Configuration
! ------------------------------------------------------------------------------

! Solarized color scheme for the X Window System
!
! http://ethanschoonover.com/solarized


! Common

#define S_yellow        #b58900
#define S_orange        #cb4b16
#define S_red           #dc322f
#define S_magenta       #d33682
#define S_violet        #6c71c4
#define S_blue          #268bd2
#define S_cyan          #2aa198
#define S_green         #859900


! Dark

#define S_base03        #002b36
#define S_base02        #073642
#define S_base01        #586e75
#define S_base00        #657b83
#define S_base0         #839496
#define S_base1         #93a1a1
#define S_base2         #eee8d5
#define S_base3         #fdf6e3


! Light

! #define S_base03        #fdf6e3
! #define S_base02        #eee8d5
! #define S_base01        #93a1a1
! #define S_base00        #839496
! #define S_base0         #657b83
! #define S_base1         #586e75
! #define S_base2         #073642
! #define S_base3         #002b36


! To only apply colors to your terminal, for example, prefix
! the color assignment statement with its name. Example:
!
! URxvt*background:            S_base03

*background:              S_base03
*foreground:              S_base0
*fading:                  40
*fadeColor:               S_base03
*cursorColor:             S_base1
*pointerColorBackground:  S_base01
*pointerColorForeground:  S_base1

*color0:                  S_base02
*color1:                  S_red
*color2:                  S_green
*color3:                  S_yellow
*color4:                  S_blue
*color5:                  S_magenta
*color6:                  S_cyan
*color7:                  S_base2
*color9:                  S_orange
*color8:                  S_base03
*color10:                 S_base01
*color11:                 S_base00
*color12:                 S_base0
*color13:                 S_violet
*color14:                 S_base1
*color15:                 S_base3


! ------------------------------------------------------------------------------
! Font configuration
! ------------------------------------------------------------------------------

!URxvt*font:		xft:M+ 1mn:regular:size=12
!URxvt*boldFont:		xft:M+ 1mn:bold:size=12
!URxvt*italicFont:	xft:M+ 1mn:italic:size=12
!URxvt*boldItalicFont:	xft:M+ 1mn:bold italic:size=12

URxvt*font:		xft:MesloLGS NF:regular:size=12
URxvt*boldFont:		xft:MesloLGS NF:bold:size=12
URxvt*italicFont:	xft:MesloLGS NF:italic:size=12
URxvt*boldItalicFont:	xft:MesloLGS NF:bold italic:size=12

! Xterm
XTerm*vt100.faceName:   xft:MesloLGS NF:size=12:antialias=true:style=Regular
XTerm*vt100.boldFont:   xft:MesloLGS NF:size=12:antialias=true:style=Bold

! ------------------------------------------------------------------------------
! Xft Font Configuration
! ------------------------------------------------------------------------------

Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintslight
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb

! ------------------------------------------------------------------------------
! URxvt configs
! ------------------------------------------------------------------------------

! font spacing
URxvt*letterSpace:		0
URxvt.lineSpace:		0

! general settings
URxvt*saveline:			15000
URxvt*termName:			rxvt-256color
URxvt*iso14755:			false
URxvt*urgentOnBell:		true

! appearance
URxvt*depth:			24
URxvt*scrollBar:		false
URxvt*scrollBar_right:	false
URxvt*internalBorder:	24
URxvt*externalBorder:	0
URxvt.geometry:			84x22

! perl extensions
URxvt.perl-ext-common:	default,clipboard,url-select,keyboard-select

! macros for clipboard and selection
URxvt.copyCommand:		xclip -i -selection clipboard
URxvt.pasteCommand:		xclip -o -selection clipboard
URxvt.keysym.M-c:		perl:clipboard:copy
URxvt.keysym.M-v:		perl:clipboard:paste
URxvt.keysym.M-C-v:		perl:clipboard:paste_escaped
URxvt.keysym.M-Escape:	perl:keyboard-select:activate
URxvt.keysym.M-s:		perl:keyboard-select:search
URxvt.keysym.M-u:		perl:url-select:select_next
URxvt.urlLauncher:		firefox
URxvt.underlineURLs:	true
URxvt.urlButton:		1

! scroll one line
URxvt.keysym.Shift-Up:		command:\033]720;1\007
URxvt.keysym.Shift-Down:	command:\033]721;1\007

! control arrow
URxvt.keysym.Control-Up:	\033[1;5A
URxvt.keysym.Control-Down:	\033[1;5B
URxvt.keysym.Control-Right:	\033[1;5C
URxvt.keysym.Control-Left:	\033[1;5D

! ------------------------------------------------------------------------------
! Rofi configs
! ------------------------------------------------------------------------------

!rofi.color-enabled: true
!rofi.color-window: #2e3440, #2e3440, #2e3440
!rofi.color-normal: #2e3440, #d8dee9, #2e3440, #2e3440, #bf616a
!rofi.color-active: #2e3440, #b48ead, #2e3440, #2e3440, #93e5cc
!rofi.color-urgent: #2e3440, #ebcb8b, #2e3440, #2e3440, #ebcb8b
!rofi.modi: run,drun,window

! ------------------------------------------------------------------------------
! Dmenu configs
! ------------------------------------------------------------------------------

!dmenu.selforeground:	    #d8dee9
!dmenu.background:	        #2e3440
!dmenu.selbackground:	    #bf616a
!dmenu.foreground:	        #d8dee9

