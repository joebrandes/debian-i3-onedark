#!/bin/bash
# install it ALL - on your own ... - i hope you know what you are doing ;-)
# just ssh in your new Machine and copy & Paste or git clone this Repo before

# openSUSE 15.4 - Serverinstall
#
# Diffs to original install.sh for Vanilla Debian 11
# only i3lock (instead of i3lock-fancy)
# python3-devel (instead of python3-dev)
# zathura-plugin-pdf-poppler zathura-plugin-ps (instead of zathura-pdf-poppler zathura-ps)
# python3-Pygments (instead of python3-pygments)
# MozillaFirefox (instead of firefox-esr)
# xorg-x11 (instead of xorg)
# rxvt-unicode (instead of rxvt)
# fontawesome-fonts (instead of fonts-font-awesome)
# noto-* (instead of fonts-noto)
# libjpeg8 (instead of libjpeg-dev)
# zlib-devel (????)  (instead of zlib1g-dev)
# libXtst-devel (instead of libxtst-dev)
# suckless-tools , yad  (missing right now)
# PLUS: gcc, python3-importlib_resources
# startx needs .xinitrc (instead of .xsession)

# Update your system
sudo zypper refresh && sudo zypper update -y

# Software installs
sudo zypper install xorg-x11 xinit xsettingsd xterm \
    i3 i3status i3lock python3-i3ipc python3-pygit2 python3-netifaces \
    MozillaFirefox evolution evince vlc geany \
    rxvt-unicode kitty rofi dzen2 picom dunst \
    ranger python3-pip python3-devel libjpeg8 zlib-devel libXtst-devel \
    zsh fzf ripgrep python3-Pygments neofetch \
    noto-* fontawesome-fonts gcc python3-importlib_resources \
    highlight hsetroot scrot htop btop calc figlet arandr nitrogen inxi \
    pcmanfm thunar lxappearance mc vim git tmux curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior \
    xdotool zathura zathura-plugin-pdf-poppler zathura-plugin-ps

# Polybar from X11:Utilities Repo
sudo zypper addrepo https://download.opensuse.org/repositories/X11:Utilities/15.4/X11:Utilities.repo
sudo zypper refresh
sudo zypper install polybar

# install ueberzug
pip3 install ueberzug

# install bumblebee-status
pip3 install bumblebee-status

# install autotiling
pip3 install autotiling

# Starship
curl -sS https://starship.rs/install.sh | sh

# German X11 Layout
sudo localectl set-x11-keymap de

# Reboot
sudo reboot

