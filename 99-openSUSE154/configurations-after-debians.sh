#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# you can use the standard conifgs: K-confs-debian-nearly-all.sh
# BUT: openSUSE needs ~/.xinitrc instead of ~/.xsessionrc and ~/.xsession

# openSUSE startx wants no .xsession!
cp ../10-Xorg-Resources/.xsessionrc ~/.xinitrc
# Get i3 started
echo 'exec i3' >> ~/.xinitrc

mv ~/.xsession ~/.xsession-deb
mv ~/.xsessionrc ~/.xsessionrc-deb

