# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/joeb/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#

# ANTIGEN Solution for Plugins:
# =============================================================================
# https://github.com/zsh-users/antigen
# https://github.com/zsh-users/antigen/wiki/Quick-start
# curl -L git.io/antigen > antigen.zsh
# or use git.io/antigen-nightly for the latest version
source $HOME/antigen.zsh
# antigen commands: antigen help
# =============================================================================
#
antigen use oh-my-zsh
# plugins in:  ~/.antigen/bundles/robbyrussel/oh-my-zsh/...
antigen bundle git
antigen bundle colorize
antigen bundle colored-man-pages
antigen bundle alias-finder
# plugins in: ~/.antigen/bundles/zsh-users/...
antigen bundle zsh-users/zsh-completions
antigen bundle z
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

# apply bundles
antigen apply
# =============================================================================



# FZF - RIPGREP
# =============================================================================
# https://github.com/junegunn/fzf
# fzf environment and exports:
# Original Debian:
# . /usr/share/doc/fzf/examples/key-bindings.zsh
# . /usr/share/doc/fzf/examples/completion.zsh
. $HOME/.config/fzf/examples/key-bindings.zsh
. $HOME/.config/fzf/examples/completion.zsh
# not good to put preview to FZF_DEFAULT_OPTS - look down to other opts
export FZF_DEFAULT_OPTS='--height 75% --border'
# use ripgrep with FZF instead of find
# export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'

# more commands:
export FZF_ALT_C_COMMAND='find . -type d'
# Print tree structure in the preview window
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"
# Preview file content using bat/batcat (https://github.com/sharkdp/bat)
# if Alt C does not work in my Terminal just tweak it to the damning symbol
bindkey -s 'ã' "fdfind --type d . --hidden | fzf --preview 'tree -C {}'^M"

export FZF_CTRL_T_OPTS="
  --preview 'batcat --style numbers,changes --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# configuration for ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# ============================================================================
# CDD - Change Directory with Dialogue
alias cddhome="cd ~ && cd \$(find . -type d | fzf)"
alias cddprojects="cd /var/www/html && cd \$(find * -type d | fzf)"
alias nf="nvim \$(rg --files --hidden -g !.git | fzf)"
alias nd="nvim \$(find . -type d | fzf)"

# ZSH-Plugin COLORIZE - see Chapter with antigen above
# =============================================================================
# provides tools colorize_cat (alias ccat)
# and colorize_less (cless):
#
if [ -n "$(command -v colorize_cat)" ] && [ -n "$(command -v colorize_less)" ]; then
    alias cat='colorize_cat'
    alias less='colorize_less'
fi
# =============================================================================


# EXPORTS
# put my Exports here - later in extra file?
# =============================================================================
#
# ZSH Colorize Style
export ZSH_COLORIZE_STYLE="native"

# fixing Exports for RANGER standard Editor vim or nvim
export VISUAL=vim
export EDITOR=vim

# STARSHIP.RS
# =============================================================================
#
eval "$(starship init zsh)"
