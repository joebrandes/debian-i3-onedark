#!/bin/bash
# Again: i hope you know what you are doing ;-)
# I act as if beeing in the repo dir like ~/gitlab-projekte/debian-i3-onedark

# Prepare local folder
mkdir ~/bin
mkdir ~/.local/bin
mkdir ~/opt
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/tmp


# CP i3 stuff
cp -R ../00-WM-i3-i3block-i3status/.config/i3* ~/.config/
# cp ../00-WM-i3-i3block-i3status/.local/bin/autotiling ~/.local/bin/
echo 'i3 stuff copied'

# CP Bumblebee-Status stuff
cp -R ../05-bumblebee-status/.config/bumblebee-status ~/.config/
echo 'Bumblebee-Status stuff copied'

# CP picom and compton conf
cp ../10-picom/.config/picom.conf ~/.config/
cp ../10-compton/.config/compton.conf ~/.config/
echo 'Picom (and Compton) conf copied'

# Xorg
cp ../10-Xorg-Resources/.Xresources* ~
cp ../10-Xorg-Resources/.xsession* ~
cp ../10-Xorg-Resources/.xinit* ~
echo 'Xorg Resouces copied'

# Bash
cp ../20-bash/.bashrc ~
echo '.bashrc copied'

# Zsh
curl -L git.io/antigen > ~/antigen.zsh
echo 'Antigen plugin manager curled'
cp ../20-zsh/.zshrc ~
echo '.zshrc copied'

# Tmux
mkdir -p ~/.tmux/plugins
git clone https://github.com/wfxr/tmux-power.git ~/.tmux/plugins/tmux-power
# Change with my tmux-power.tmux conf
cp ~/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux-original
cp ../30-tmux/.tmux/plugins/tmux-power/tmux-power.tmux ~/.tmux/plugins/tmux-power/tmux-power.tmux
# do not forget your ~/.tmux.conf
cp ../30-tmux/.tmux.conf ~/.tmux.conf


# Kitty
cp -R ../40-kitty/.config/kitty/ ~/.config/
cp -R ../40-kitty/.local/share/* ~/.local/share/
echo 'Kitty conf copied'

# Urxvt
cp -R ../40-urxvt/.urxvt/ ~
echo 'Urxvt stuff copied'

# FZF
cp -R ../50-fzf/.config/fzf ~/.config/
echo 'FZF stuff copied'

# Starship
cp ../50-starship/.config/starship.toml ~/.config/
echo 'Starship.toml copied'

# Vim
cp ../60-vim/.vimrc ~
cp -R ../60-vim/.vim ~
echo 'Vim stuff copied'

# Btop
cp -R ../70-btop/.config/* ~/.config/
echo 'Btop stuff copied'

# MC
mkdir -p ~/.local/share/mc/skins 2>/dev/null
cp -R ../70-mc/.local/share/mc/skins/onedark.ini ~/.local/share/mc/skins
cp -R ../70-mc/.config/mc ~/.config/
echo 'MC stuff copied'

# Neovim
cp -R ../70-neovim/.config/nvim ~/.config

# Ranger
cp -R ../70-ranger/.config/ranger ~/.config/
echo 'Ranger stuff copied'

# Rofi
cp -R ../70-rofi/.config/rofi ~/.config/
echo 'Rofi conf copied'

# Fonts - no folder .fonts in naked system
cp -R ../90-FONTS/.fonts/* ~/.fonts
cp -R ../90-FONTS/.local/share/fonts/* ~/.local/share/fonts/
echo 'MyFonts for ~/.fonts copied'

# Pictures
mkdir ~/pictures
cp -R ../90-PICS/* ~/pictures
echo 'Pictures copied'
