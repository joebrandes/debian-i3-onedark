# Oh My Posh

A nice alternative for prompt/powerline handling on various
Operating Systems

*   [Oh My Posh - Website](https://ohmyposh.dev/)
*   [Oh My Posh - Github Repo Jan de Dobbeleer](https://github.com/jandedobbeleer/oh-my-posh)

<img src="./logo-ohmyposh.png" alt= "Oh My Posh Logo" width="50%">


Installation: https://ohmyposh.dev/docs/installation/linux

There is installation available for system or user:

    curl -s https://ohmyposh.dev/install.sh | bash -s
    curl -s https://ohmyposh.dev/install.sh | bash -s -- -d ~/bin



## Install [2023-05-11]

[Installation Linux Oh My Posh](https://ohmyposh.dev/docs/installation/linux)

Can easily be done per copy & paste like above - BUT...

Problems (on Debian / Linux Mint for example):

The Script use ``uname -m`` to detect the architecture of system.
The arch ``x86_64`` is not recognized!

Solution: put Code in detect_arch() function


    if [ "${arch}" = "x86_64" ] && [ "$(getconf LONG_BIT)" -eq 64 ]; then
        arch=amd64
    fi

**Modified Installerscript** provided for this folder: ``ohmyposh-install.sh``

Can be used with ``bash ohmyposh-install.sh -d ~/bin``

## Configure your Shell

[Configure Shell for Oh My Posh](https://ohmyposh.dev/docs/installation/prompt)

Put the following line in your ``.bashrc``

    eval "$(oh-my-posh init bash)"

Put the following line in your ``.zshrc``

    eval "$(oh-my-posh init zsh)"


## Configure Oh My Posh

[Config Syntax for Oh My Posh](https://ohmyposh.dev/docs/installation/customize)

For Example:

    eval "$(oh-my-posh init bash --config ~./cache/oh-my-posh/themes/jandedobbeleer.omp.json)"

These Themes got downloaded (and unzipped) from Main Repo during install
procedure and can easily be modified.

[Configuration Guide Oh My Posh](https://ohmyposh.dev/docs/configuration/overview)


## Show Style

Live Preview of own config

    oh-my-posh print primary --config ~/.cache/oh-my-posh/themes/rudolfs-dark.omp.json --shell uni

## Combinations with PowerShell configs

Because i often use PowerShell (on various OS) my intentions are the
production of same Env for all. And in Windows i am often limited
regarding installations. So i often rely on the Executable Binaries
provided by Developers.

In my **PowerShell Profile** u can find such provisions for Oh-My-Posh.

*   E.g. special Themes for Oh-My-Posh in Folder ``jb_tools_linus``
