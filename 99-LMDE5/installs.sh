#!/bin/bash
# install it ALL - on your own ... - i hope you know what you are doing ;-)
# just ssh in your new Machine and copy & Paste or git clone this Repo before
#
# Linux Mint Debian Edition 5
#
# Diffs to original install.sh for Vanilla Debian 11
# no firefox-esr
# different approach for polybar

# Update your system
sudo apt update && sudo apt upgrade -y

# Software installs
sudo apt install xorg xinit xsettingsd xterm  aptitude \
    i3 i3status i3lock-fancy python3-i3ipc  \
    evolution evince vlc geany gsimplecal \
    rxvt kitty rofi suckless-tools dzen2 picom dunst \
    ranger python3-pip python3-dev python3-pygit2 \
    python3-psutil python3-netifaces gnome-system-monitor \
    libjpeg-dev zlib1g-dev libxtst-dev \
    zsh fzf ripgrep python3-pygments python3-venv neofetch \
    fonts-noto fonts-font-awesome \
    highlight hsetroot scrot htop calc figlet arandr nitrogen inxi \
    pcmanfm thunar lxappearance mc vim git tmux curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior \
    yad xdotool xcwd zathura zathura-pdf-poppler zathura-ps

# Polybar from Backports LMDE5
sudo apt install -t bullseye-backports polybar

# install ueberzug
pip3 install ueberzug

# install bumblebee
pip3 install bumblebee-status

# install autotiling
pip3 install autotiling

# Starship
curl -sS https://starship.rs/install.sh | sh

# Reboot
sudo reboot

